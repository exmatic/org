package vacationgrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/vacation"
	"gitlab.com/exmatic/org/business/core/vacation/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of Vacation endpoints.
type Handlers struct {
	Vacation vacation.Core
}

// Delete removes a Vacation from the system.
// @Summary Delete vacation
// @Security ApiKeyAuth
// @Tags vacation
// @Description removes a vacation from the system
// @Accept  json
// @Produce json
// @Param id path string true "vacation ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /vacation/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewVacationError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Vacation.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this Vacation is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying Vacation[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a Vacation you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewVacationError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Vacation.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of companies with paging.
// @Summary Query vacation
// @Security ApiKeyAuth
// @Tags vacation
// @Description returns a list of vacation with paging
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Param sort query string false "supported sort values: date_since (ascending order) and -date_since (descending order)"
// @Param department_id query string false "department_id"
// @Param request_type_id query string false "request_type_id"
// @Param user_id query string false "user_id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /vacation/{page}/{rows}/{company_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	var input struct {
		FilterVacation db.FilterVacation
		filter.Filters
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "company_id")

	// call r.URL.Query() to get the url.Values map containing the query string data.
	qs := r.URL.Query()

	input.Filters.PageNumber = pageNumber
	input.Filters.RowsPerPage = rowsPerPage
	input.Filters.Sort = web.ReadString(qs, "sort", "-date_since")

	input.FilterVacation.DepartmentID = web.ReadString(qs, "department_id", "")
	input.FilterVacation.RequestTypeID = web.ReadString(qs, "request_type_id", "")
	input.FilterVacation.UserID = web.ReadString(qs, "user_id", "")

	// Add the supported sort value for this endpoint to the sort safelist.
	input.Filters.SortSafeList = []string{
		// ascending sort values
		"date_since",
		// descending sort values
		"-date_since",
	}

	xx, err := h.Vacation.Query(ctx, cid, input.Filters, input.FilterVacation)
	if err != nil {
		return fmt.Errorf("unable to query for companies: %w", err)
	}

	return web.Respond(ctx, w, xx, http.StatusOK)
}

// QueryByID returns a Vacation by its ID.
// @Summary QueryByID vacation
// @Security ApiKeyAuth
// @Tags vacation
// @Description returns a vacation by its ID
// @Accept  json
// @Produce json
// @Param id path string true "vacation ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /vacation/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Vacation.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
