package taskgrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/task"
	"gitlab.com/exmatic/org/business/core/task/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of Task endpoints.
type Handlers struct {
	Task task.Core
}

// Create adds a new Task to the system.
// @Summary Create Task
// @Security ApiKeyAuth
// @Tags Task
// @Description adds a new Task to the system
// @Accept  json
// @Produce json
// @Param input body task.NewTask true "position info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /task [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx task.NewTask
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.Task.Create(ctx, nx, v.Now)
	if err != nil {
		return fmt.Errorf("creating new Task, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Update updates a Task in the system.
// NOTE: only HR can update Task status
// @Summary Update Task
// @Security ApiKeyAuth
// @Tags Task
// @Description Update updates a Task in the system
// @Accept  json
// @Produce json
// @Param id path string true "Task ID"
// @Param input body task.UpdateTask true "position info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadTask"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /task/{id} [patch]
func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd task.UpdateTask
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.Task.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying Task[%s]: %w", id, err)
		}
	}

	if err := h.Task.Update(ctx, id, upd); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] Department[%+v]: %w", id, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Delete removes a Task from the system.
// @Summary Delete Task
// @Security ApiKeyAuth
// @Tags Task
// @Description removes a Task from the system
// @Accept  json
// @Produce json
// @Param id path string true "Task ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadTask"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /task/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Task.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this Task is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying Task[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a Task you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Task.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of companies with paging.
// @Summary Query Task
// @Security ApiKeyAuth
// @Tags Task
// @Description returns a list of Tasks with paging/sorts/filtering
// @Description example of a query with sorts and filters:
// @Description localhost:3000/v1/Task/{page}/{rows}/{uuid_of_company}?sort=-date_created&department_id={uuid}&Task_status={in progress/approved/declined}&user_id={uuid}
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Param sort query string false "supported sort values: date_created (ascending order) and -date_created (descending order)"
// @Param department_id query string false "department_id"
// @Param project_id query string false "project_id"
// @Param team_id query string false "team_id"
// @Param assigned_by query string false "assigned_by"
// @Param assigned_to query string false "assigned_to"
// @Param task_importance_id query string false "task_importance_id"
// @Param task_status_id query string false "task_status_id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadTask"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /task/{page}/{rows}/{company_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	var input struct {
		FilterTask db.FilterTask
		filter.Filters
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "company_id")

	// call r.URL.Query() to get the url.Values map containing the query string data.
	qs := r.URL.Query()

	input.Filters.PageNumber = pageNumber
	input.Filters.RowsPerPage = rowsPerPage
	input.Filters.Sort = web.ReadString(qs, "sort", "-date_created")

	input.FilterTask.DepartmentID = web.ReadString(qs, "department_id", "")
	input.FilterTask.ProjectID = web.ReadString(qs, "project_id", "")
	input.FilterTask.TeamID = web.ReadString(qs, "team_id", "")
	input.FilterTask.AssignedBy = web.ReadString(qs, "assigned_by", "")
	input.FilterTask.AssignedTo = web.ReadString(qs, "assigned_to", "")
	input.FilterTask.TaskImportanceID = web.ReadString(qs, "task_importance_id", "")
	input.FilterTask.TaskStatusID = web.ReadString(qs, "task_status_id", "")

	// Add the supported sort value for this endpoint to the sort safelist.
	input.Filters.SortSafeList = []string{
		// ascending sort values
		"date_created",
		// descending sort values
		"-date_created",
	}

	xx, err := h.Task.Query(ctx, cid, input.Filters, input.FilterTask)
	if err != nil {
		return fmt.Errorf("unable to query for companies: %w", err)
	}

	return web.Respond(ctx, w, xx, http.StatusOK)
}

// QueryByID returns a Task by its ID.
// @Summary QueryByID Task
// @Security ApiKeyAuth
// @Tags Task
// @Description returns a Task by its ID
// @Accept  json
// @Produce json
// @Param id path string true "Task ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadTask"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /task/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Task.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
