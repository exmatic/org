package candidategrp

import (
	"context"
	"errors"
	"fmt"
	"github.com/lib/pq"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/candidate"
	"gitlab.com/exmatic/org/business/core/candidate/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of Candidate endpoints.
type Handlers struct {
	Candidate candidate.Core
}

// Create adds a new Candidate to the system.
// @Summary Create candidate
// @Security ApiKeyAuth
// @Tags candidate
// @Description adds a new candidate to the system
// @Accept  json
// @Produce json
// @Param input body candidate.NewCandidate true "candidate info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /candidate [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx candidate.NewCandidate
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.Candidate.Create(ctx, nx, v.Now)
	if err != nil {
		return fmt.Errorf("creating new Candidate, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Update updates a Candidate in the system.
// NOTE: only HR can update Candidate status
// @Summary Update candidate
// @Security ApiKeyAuth
// @Tags candidate
// @Description Update updates a candidate in the system
// @Accept  json
// @Produce json
// @Param id path string true "candidate ID"
// @Param input body candidate.UpdateCandidate true "candidate info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /candidate/{id} [patch]
func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd candidate.UpdateCandidate
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.Candidate.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying Candidate[%s]: %w", id, err)
		}
	}

	if err := h.Candidate.Update(ctx, id, upd); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] Department[%+v]: %w", id, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Delete removes a Candidate from the system.
// @Summary Delete candidate
// @Security ApiKeyAuth
// @Tags candidate
// @Description removes a candidate from the system
// @Accept  json
// @Produce json
// @Param id path string true "candidate ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /candidate/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Candidate.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this Candidate is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying Candidate[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a Candidate you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Candidate.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// @Summary Query candidate
// @Tags candidate
// @Security ApiKeyAuth
// @Description returns a list of candidates with paging
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Param sort query string false "supported sort values: date_created (ascending order) and -date_created (descending order)"
// @Param creator_id query string false "creator_id"
// @Param skills query []string false "array of skills"
// @Param vacancy_id query string false "vacancy_id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /candidate/{page}/{rows}/{company_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	var input struct {
		db.FilterCandidate
		filter.Filters
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "company_id")

	// call r.URL.Query() to get the url.Values map containing the query string data.
	qs := r.URL.Query()

	input.Filters.PageNumber = pageNumber
	input.Filters.RowsPerPage = rowsPerPage
	input.Filters.Sort = web.ReadString(qs, "sort", "-date_created")

	input.FilterCandidate.CreatorID = web.ReadString(qs, "creator_id", "")
	input.FilterCandidate.Skills = pq.StringArray(web.ReadCSV(qs, "skills", pq.StringArray{}))
	input.FilterCandidate.VacancyID = web.ReadString(qs, "vacancy_id", "")

	// Add the supported sort value for this endpoint to the sort safelist.
	input.Filters.SortSafeList = []string{
		// ascending sort values
		"date_created",
		// descending sort values
		"-date_created",
	}

	xx, err := h.Candidate.Query(ctx, cid, input.Filters, input.FilterCandidate)
	if err != nil {
		return fmt.Errorf("unable to query for companies: %w", err)
	}

	return web.Respond(ctx, w, xx, http.StatusOK)
}

// @Summary QueryByID candidate
// @Security ApiKeyAuth
// @Tags candidate
// @Description returns a candidate by its ID
// @Accept  json
// @Produce json
// @Param id path string true "candidate ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /candidate/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Candidate.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
