package requestgrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/request"
	"gitlab.com/exmatic/org/business/core/request/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of Request endpoints.
type Handlers struct {
	Request request.Core
}

// Create adds a new Request to the system.
// @Summary Create request
// @Security ApiKeyAuth
// @Tags request
// @Description adds a new request to the system
// @Accept  json
// @Produce json
// @Param input body request.NewRequest true "position info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /request [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx request.NewRequest
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.Request.Create(ctx, nx, v.Now)
	if err != nil {
		return fmt.Errorf("creating new Request, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Update updates a Request in the system.
// NOTE: only HR can update request status
// @Summary Update request
// @Security ApiKeyAuth
// @Tags request
// @Description Update updates a request in the system
// @Accept  json
// @Produce json
// @Param id path string true "request ID"
// @Param input body request.UpdateRequest true "position info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request/{id} [patch]
func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd request.UpdateRequest
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.Request.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying Request[%s]: %w", id, err)
		}
	}

	if err := h.Request.Update(ctx, id, upd); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] Department[%+v]: %w", id, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// @Summary UpdateRequestStatus Request
// @Security ApiKeyAuth
// @Tags request
// @Description UpdateRequestStatus updates a request status in the system
// @Accept  json
// @Produce json
// @Param id path string true "Request ID"
// @Param input body request.UpdateRequestStatus true "request status info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request/{id} [put]
func (h Handlers) UpdateRequestStatus(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	// NOTE: only HR can update requestStatus
	// if !claims.Authorized(auth.RoleHR) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd request.UpdateRequestStatus
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.Request.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying Request[%s]: %w", id, err)
		}
	}

	vacationID, err := h.Request.UpdateRequestStatus(ctx, id, upd)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		case errors.Is(err, core.ErrUnsupportedRequestStatus):
			return v1Web.NewRequestError(err, http.StatusUnprocessableEntity)
		default:
			return fmt.Errorf("ID[%s] Department[%+v]: %w", id, &upd, err)
		}
	}

	if vacationID == "" {
		return web.Respond(ctx, w, nil, http.StatusNoContent)
	}

	return web.Respond(ctx, w, vacationID, http.StatusOK)
}

// Delete removes a Request from the system.
// @Summary Delete request
// @Security ApiKeyAuth
// @Tags request
// @Description removes a request from the system
// @Accept  json
// @Produce json
// @Param id path string true "request ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Request.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this Request is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying Request[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a Request you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Request.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of companies with paging.
// @Summary Query request
// @Security ApiKeyAuth
// @Tags request
// @Description returns a list of requests with paging/sorts/filtering
// @Description example of a query with sorts and filters:
// @Description localhost:3000/v1/request/{page}/{rows}/{uuid_of_company}?sort=-date_created&department_id={uuid}&request_status={in progress/approved/declined}&user_id={uuid}
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Param sort query string false "supported sort values: date_created (ascending order) and -date_created (descending order)"
// @Param department_id query string false "department_id"
// @Param request_type_id query string false "request_type_id"
// @Param request_status query string false "supported values: in progress/approved/declined"
// @Param user_id query string false "user_id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request/{page}/{rows}/{company_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	var input struct {
		FilterRequest db.FilterRequest
		filter.Filters
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "company_id")

	// call r.URL.Query() to get the url.Values map containing the query string data.
	qs := r.URL.Query()

	input.Filters.PageNumber = pageNumber
	input.Filters.RowsPerPage = rowsPerPage
	input.Filters.Sort = web.ReadString(qs, "sort", "-date_created")

	input.FilterRequest.DepartmentID = web.ReadString(qs, "department_id", "")
	input.FilterRequest.RequestTypeID = web.ReadString(qs, "request_type_id", "")
	input.FilterRequest.RequestStatus = web.ReadString(qs, "request_status", "")
	input.FilterRequest.UserID = web.ReadString(qs, "user_id", "")

	// Add the supported sort value for this endpoint to the sort safelist.
	input.Filters.SortSafeList = []string{
		// ascending sort values
		"date_created",
		// descending sort values
		"-date_created",
	}

	xx, err := h.Request.Query(ctx, cid, input.Filters, input.FilterRequest)
	if err != nil {
		return fmt.Errorf("unable to query for companies: %w", err)
	}

	return web.Respond(ctx, w, xx, http.StatusOK)
}

// QueryByID returns a Request by its ID.
// @Summary QueryByID request
// @Security ApiKeyAuth
// @Tags request
// @Description returns a request by its ID
// @Accept  json
// @Produce json
// @Param id path string true "request ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Request.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
