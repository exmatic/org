package complaintgrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/complaint"
	"gitlab.com/exmatic/org/business/sys/filter"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of Complaint endpoints.
type Handlers struct {
	Complaint complaint.Core
}

// Create adds a new Complaint to the system.
// @Summary Create complaint
// @Security ApiKeyAuth
// @Tags complaint
// @Description adds a new complaint to the system
// @Accept  json
// @Produce json
// @Param input body complaint.NewComplaint true "complaint info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /complaint [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx complaint.NewComplaint
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.Complaint.Create(ctx, nx, v.Now)
	if err != nil {
		return fmt.Errorf("creating new Complaint, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Delete removes a Complaint from the system.
// @Summary Delete complaint
// @Security ApiKeyAuth
// @Tags complaint
// @Description removes a complaint from the system
// @Accept  json
// @Produce json
// @Param id path string true "complaint ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /complaint/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewComplaintError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Complaint.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this Complaint is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying Complaint[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a Complaint you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewComplaintError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Complaint.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of complaints with paging.
// @Summary Query complaint
// @Security ApiKeyAuth
// @Tags complaint
// @Description returns a list of complaints with paging
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /complaint/{page}/{rows} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	var input struct {
		filter.Filters
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "company_id")

	// call r.URL.Query() to get the url.Values map containing the query string data.
	qs := r.URL.Query()

	input.Filters.PageNumber = pageNumber
	input.Filters.RowsPerPage = rowsPerPage
	input.Filters.Sort = web.ReadString(qs, "sort", "-date_created")

	// Add the supported sort value for this endpoint to the sort safelist.
	input.Filters.SortSafeList = []string{
		// ascending sort values
		"date_created",
		// descending sort values
		"-date_created",
	}

	xx, err := h.Complaint.Query(ctx, cid, input.Filters)
	if err != nil {
		return fmt.Errorf("unable to query for companies: %w", err)
	}

	return web.Respond(ctx, w, xx, http.StatusOK)
}

// QueryByID returns a Complaint by its ID.
// @Summary QueryByID complaint
// @Security ApiKeyAuth
// @Tags complaint
// @Description returns a complaint by its ID
// @Accept  json
// @Produce json
// @Param id path string true "complaint ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /complaint/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Complaint.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
