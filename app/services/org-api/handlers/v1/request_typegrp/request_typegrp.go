package request_typegrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/request_type"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of RequestType endpoints.
type Handlers struct {
	RequestType request_type.Core
}

// Create adds a new RequestType to the system.
// @Summary Create RequestType
// @Security ApiKeyAuth
// @Tags requestType
// @Description adds a new RequestType to the system
// @Accept  json
// @Produce json
// @Param input body request_type.NewRequestType true "request_type info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /request_type [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	_, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx request_type.NewRequestType
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.RequestType.Create(ctx, nx)
	if err != nil {
		return fmt.Errorf("creating new RequestType, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Update updates a RequestType in the system.
// @Summary Update RequestType
// @Security ApiKeyAuth
// @Tags requestType
// @Description Update updates a RequestType in the system
// @Accept  json
// @Produce json
// @Param id path string true "RequestType ID"
// @Param input body request_type.RequestType true "request_type info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request_type/{id} [patch]
func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd request_type.UpdateRequestType
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.RequestType.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying RequestType[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to update a RequestType you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.RequestType.Update(ctx, id, upd); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] RequestType[%+v]: %w", id, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Delete removes a RequestType from the system.
// @Summary Delete RequestType
// @Security ApiKeyAuth
// @Tags requestType
// @Description removes a RequestType from the system
// @Accept  json
// @Produce json
// @Param id path string true "RequestType ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request_type/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.RequestType.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this RequestType is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying RequestType[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a RequestType you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.RequestType.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of RequestType with paging.
// @Summary Query RequestType
// @Security ApiKeyAuth
// @Tags requestType
// @Description returns a list of RequestTypes with paging
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param company_id path string true "company id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request_type/{page}/{rows}/{company_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	fkID := web.Param(r, "company_id")

	RequestTypes, err := h.RequestType.Query(ctx, pageNumber, rowsPerPage, fkID)
	if err != nil {
		return fmt.Errorf("unable to query for RequestType: %w", err)
	}

	return web.Respond(ctx, w, RequestTypes, http.StatusOK)
}

// QueryByID returns a RequestType by its ID.
// @Summary QueryByID RequestType
// @Security ApiKeyAuth
// @Tags requestType
// @Description returns a RequestType by its ID
// @Accept  json
// @Produce json
// @Param id path string true "RequestType ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /request_type/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.RequestType.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
