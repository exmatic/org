package projectgrp

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/project"
	v1Web "gitlab.com/exmatic/org/business/web/v1"
	"gitlab.com/exmatic/org/foundation/web"
	"net/http"
	"strconv"
)

// Handlers manages the set of project endpoints.
type Handlers struct {
	Project project.Core
}

// Create adds a new project to the system.
// @Summary Create project
// @Security ApiKeyAuth
// @Tags project
// @Description adds a new project to the system
// @Accept  json
// @Produce json
// @Param input body project.NewProject true "project info"
// @Success 201 {integer} integer
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /project [post]
func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nx project.NewProject
	if err := web.Decode(r, &nx); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	x, err := h.Project.Create(ctx, nx, v.Now)
	if err != nil {
		return fmt.Errorf("creating new project, nx[%+v]: %w", nx, err)
	}

	return web.Respond(ctx, w, x, http.StatusCreated)
}

// Update updates a project in the system.
// @Summary Update project
// @Security ApiKeyAuth
// @Tags project
// @Description Update updates a project in the system
// @Accept  json
// @Produce json
// @Param id path string true "project ID"
// @Param input body project.UpdateProject true "project info"
// @Success 204 {integer} integer "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /project/{id} [patch]
func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//v, err := web.GetValues(ctx)
	//if err != nil {
	//	return web.NewShutdownError("web value missing from context")
	//}

	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	var upd project.UpdateProject
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	id := web.Param(r, "id")

	_, err := h.Project.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying project[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to update a project you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Project.Update(ctx, id, upd); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] project[%+v]: %w", id, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Delete removes a project from the system.
// @Summary Delete project
// @Security ApiKeyAuth
// @Tags project
// @Description removes a project from the system
// @Accept  json
// @Produce json
// @Param id path string true "project ID"
// @Success 204 {integer} integer
// @Failure 204 {object} v1.ErrorResponse "httpStatusNoContent"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /project/{id} [delete]
func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	//claims, err := auth.GetClaims(ctx)
	//if err != nil {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	id := web.Param(r, "id")

	_, err := h.Project.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):

			// Don't send StatusNotFound here since the call to Delete
			// below won't if this project is not found. We only know
			// this because we are doing the Query for the UserID.
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying project[%s]: %w", id, err)
		}
	}

	//// If you are not an admin and looking to delete a project you don't own.
	//if !claims.Authorized(auth.RoleAdmin) {
	//	return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	//}

	if err := h.Project.Delete(ctx, id); err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Query returns a list of projects with paging.
// @Summary Query project
// @Security ApiKeyAuth
// @Tags project
// @Description returns a list of projects with paging
// @Accept  json
// @Produce json
// @Param page path int true "page number to show"
// @Param rows path int true "rows per page"
// @Param department_id path string true "department id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /project/{page}/{rows}/{department_id} [get]
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format, page[%s]", page), http.StatusBadRequest)
	}
	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format, rows[%s]", rows), http.StatusBadRequest)
	}
	cid := web.Param(r, "department_id")

	projects, err := h.Project.Query(ctx, pageNumber, rowsPerPage, cid)
	if err != nil {
		return fmt.Errorf("unable to query for projects: %w", err)
	}

	return web.Respond(ctx, w, projects, http.StatusOK)
}

// QueryByID returns a project by its ID.
// @Summary QueryByID project
// @Security ApiKeyAuth
// @Tags project
// @Description returns a project by its ID
// @Accept  json
// @Produce json
// @Param id path string true "project ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 404 {object} v1.ErrorResponse "httpStatusNotFound"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /project/{id} [get]
func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")
	x, err := h.Project.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, core.ErrInvalidID):
			return v1Web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, core.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, x, http.StatusOK)
}
