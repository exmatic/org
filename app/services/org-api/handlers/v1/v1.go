// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/candidategrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/companygrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/complaintgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/departmentgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/gradegrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/invitegrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/positionsgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/projectgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/request_typegrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/requestgrp"
	taskgrp "gitlab.com/exmatic/org/app/services/org-api/handlers/v1/task"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/task_importancegrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/task_statusgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/teamgrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/vacancygrp"
	"gitlab.com/exmatic/org/app/services/org-api/handlers/v1/vacationgrp"
	"gitlab.com/exmatic/org/business/core/candidate"
	"gitlab.com/exmatic/org/business/core/company"
	"gitlab.com/exmatic/org/business/core/complaint"
	"gitlab.com/exmatic/org/business/core/department"
	"gitlab.com/exmatic/org/business/core/grade"
	"gitlab.com/exmatic/org/business/core/invite"
	"gitlab.com/exmatic/org/business/core/position"
	"gitlab.com/exmatic/org/business/core/project"
	"gitlab.com/exmatic/org/business/core/request"
	"gitlab.com/exmatic/org/business/core/request_type"
	"gitlab.com/exmatic/org/business/core/role"
	"gitlab.com/exmatic/org/business/core/task"
	"gitlab.com/exmatic/org/business/core/task_importance"
	"gitlab.com/exmatic/org/business/core/task_status"
	"gitlab.com/exmatic/org/business/core/team"
	userdb "gitlab.com/exmatic/org/business/core/user/db"
	"gitlab.com/exmatic/org/business/core/vacancy"
	"gitlab.com/exmatic/org/business/core/vacation"
	"gitlab.com/exmatic/org/business/web/v1/mid"
	_ "gitlab.com/exmatic/org/docs"
	custom_swagger "gitlab.com/exmatic/org/foundation/custom-swagger"
	"gitlab.com/exmatic/org/foundation/web"
	"go.uber.org/zap"
	"net/http"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Log  *zap.SugaredLogger
	Auth *userdb.AuthStore
	DB   *sqlx.DB
}

// Routes binds all the version 1 routes.
func Routes(app *web.App, cfg Config) {
	const version = "v1"

	swaggerHandler := custom_swagger.Handler()

	app.Handle(http.MethodGet, version, "/swagger/*", swaggerHandler)

	authen := mid.Authenticate(*cfg.Auth)
	//roleHR := mid.Authorize(role.HR)
	roleAdmin := mid.Authorize(role.Admin)
	//roleUser := mid.Authorize(role.User)
	//roleEmployee := mid.Authorize(role.Employee)
	allRoles := mid.Authorize(role.Employee, role.HR, role.Admin)
	hrAdmin := mid.Authorize(role.HR, role.Admin)

	{
		cgh := companygrp.Handlers{
			Company: company.NewCore(cfg.Log, cfg.DB, cfg.Auth),
		}
		app.Handle(http.MethodGet, version, "/company/:page/:rows", cgh.Query)
		app.Handle(http.MethodGet, version, "/company/:id", cgh.QueryByID)
		app.Handle(http.MethodPost, version, "/company", cgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/company/:id", cgh.Update, authen, hrAdmin)
		app.Handle(http.MethodDelete, version, "/company/:id", cgh.Delete, authen, roleAdmin)
	}
	{
		dgh := departmentgrp.Handlers{
			Department: department.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/department/:page/:rows/:company_id", dgh.Query, authen, allRoles)
		app.Handle(http.MethodGet, version, "/department/:id", dgh.QueryByID, authen, allRoles)
		app.Handle(http.MethodPost, version, "/department", dgh.Create, authen, allRoles)
		app.Handle(http.MethodPatch, version, "/department/:id", dgh.Update, authen, hrAdmin)
		app.Handle(http.MethodDelete, version, "/department/:id", dgh.Delete, authen, roleAdmin)
	}
	{
		pgh := projectgrp.Handlers{
			Project: project.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/project/:page/:rows/:department_id", pgh.Query, authen)
		app.Handle(http.MethodGet, version, "/project/:id", pgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/project", pgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/project/:id", pgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/project/:id", pgh.Delete, authen)
	}
	{
		tgh := teamgrp.Handlers{
			Team: team.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/team/:page/:rows/:project_id", tgh.Query, authen)
		app.Handle(http.MethodGet, version, "/team/:id", tgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/team", tgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/team/:id", tgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/team/:id", tgh.Delete, authen)
	}
	{
		posgh := positionsgrp.Handlers{
			Position: position.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/position/:page/:rows/:company_id", posgh.Query, authen)
		app.Handle(http.MethodGet, version, "/position/:id", posgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/position", posgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/position/:id", posgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/position/:id", posgh.Delete, authen)
	}
	{
		gradegh := gradegrp.Handlers{
			Grade: grade.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/grade/:page/:rows/:company_id", gradegh.Query, authen)
		app.Handle(http.MethodGet, version, "/grade/:id", gradegh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/grade", gradegh.Create, authen)
		app.Handle(http.MethodPatch, version, "/grade/:id", gradegh.Update, authen)
		app.Handle(http.MethodDelete, version, "/grade/:id", gradegh.Delete, authen)
	}
	{
		rtgh := request_typegrp.Handlers{
			RequestType: request_type.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/request_type/:page/:rows/:company_id", rtgh.Query, authen)
		app.Handle(http.MethodGet, version, "/request_type/:id", rtgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/request_type", rtgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/request_type/:id", rtgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/request_type/:id", rtgh.Delete, authen)
	}
	vacationCore := vacation.NewCore(cfg.Log, cfg.DB)
	{
		vacationgh := vacationgrp.Handlers{
			Vacation: vacationCore,
		}
		app.Handle(http.MethodGet, version, "/vacation/:page/:rows/:company_id", vacationgh.Query, authen)
		app.Handle(http.MethodGet, version, "/vacation/:id", vacationgh.QueryByID, authen)
		app.Handle(http.MethodDelete, version, "/vacation/:id", vacationgh.Delete, authen)
		// NOTE: Vacation is created automatically when request status is 'approved' for request of type vacancy.
		// Also, updating vacancy data is not needed.
	}
	{
		requestgh := requestgrp.Handlers{
			Request: request.NewCore(cfg.Log, cfg.DB, &app.Mailer, vacationCore, cfg.Auth),
		}
		app.Handle(http.MethodGet, version, "/request/:page/:rows/:company_id", requestgh.Query, authen)
		app.Handle(http.MethodGet, version, "/request/:id", requestgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/request", requestgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/request/:id", requestgh.Update, authen)
		app.Handle(http.MethodPut, version, "/request/:id", requestgh.UpdateRequestStatus, authen)
		app.Handle(http.MethodDelete, version, "/request/:id", requestgh.Delete, authen)
	}
	{
		invitegh := invitegrp.Handlers{
			Invite: invite.NewCore(cfg.Log, cfg.DB, cfg.Auth),
		}
		app.Handle(http.MethodGet, version, "/invite/:page/:rows/:company_id", invitegh.QueryByCompanyID, authen)
		app.Handle(http.MethodGet, version, "/invite/by_email/:page/:rows/:email", invitegh.QueryByEmail, authen)
		app.Handle(http.MethodGet, version, "/invite/:id", invitegh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/invite", invitegh.Create, authen)
		app.Handle(http.MethodPut, version, "/invite/:id", invitegh.UpdateStatus, authen)
		app.Handle(http.MethodDelete, version, "/invite/:id", invitegh.Delete, authen)
	}
	{
		vacancygh := vacancygrp.Handlers{
			Vacancy: vacancy.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/vacancy/:page/:rows/:company_id", vacancygh.Query)
		app.Handle(http.MethodGet, version, "/vacancy/:id", vacancygh.QueryByID)
		app.Handle(http.MethodPost, version, "/vacancy", vacancygh.Create, authen)
		app.Handle(http.MethodPatch, version, "/vacancy/:id", vacancygh.Update, authen)
		app.Handle(http.MethodDelete, version, "/vacancy/:id", vacancygh.Delete, authen)
	}
	{
		candidategh := candidategrp.Handlers{
			Candidate: candidate.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/candidate/:page/:rows/:company_id", candidategh.Query, authen)
		app.Handle(http.MethodGet, version, "/candidate/:id", candidategh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/candidate", candidategh.Create, authen)
		app.Handle(http.MethodPatch, version, "/candidate/:id", candidategh.Update, authen)
		app.Handle(http.MethodDelete, version, "/candidate/:id", candidategh.Delete, authen)
	}
	{
		complaintgh := complaintgrp.Handlers{
			Complaint: complaint.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/complaint/:page/:rows/:company_id", complaintgh.Query, authen)
		app.Handle(http.MethodGet, version, "/complaint/:id", complaintgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/complaint", complaintgh.Create, authen)
		app.Handle(http.MethodDelete, version, "/complaint/:id", complaintgh.Delete, authen)
		// NOTE: can not update complaint
	}
	{
		xgh := task_importancegrp.Handlers{
			TaskImportance: task_importance.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/task_importance/:page/:rows/:company_id", xgh.Query, authen)
		app.Handle(http.MethodGet, version, "/task_importance/:id", xgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/task_importance", xgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/task_importance/:id", xgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/task_importance/:id", xgh.Delete, authen)
	}
	{
		xgh := task_statusgrp.Handlers{
			TaskStatus: task_status.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/task_status/:page/:rows/:company_id", xgh.Query, authen)
		app.Handle(http.MethodGet, version, "/task_status/:id", xgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/task_status", xgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/task_status/:id", xgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/task_status/:id", xgh.Delete, authen)
	}
	{
		xgh := taskgrp.Handlers{
			Task: task.NewCore(cfg.Log, cfg.DB),
		}
		app.Handle(http.MethodGet, version, "/task/:page/:rows/:company_id", xgh.Query, authen)
		app.Handle(http.MethodGet, version, "/task/:id", xgh.QueryByID, authen)
		app.Handle(http.MethodPost, version, "/task", xgh.Create, authen)
		app.Handle(http.MethodPatch, version, "/task/:id", xgh.Update, authen)
		app.Handle(http.MethodDelete, version, "/task/:id", xgh.Delete, authen)
	}
}
