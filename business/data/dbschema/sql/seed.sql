insert into company
values ('802be894-d1f3-11ec-baf3-526e5eed5702', '627d0550f40b33cd4a036585', 'company1', '87772292347',
        'sobaka@gmail.com', 'kabanbay batyra 25/2', '123', 'example.com', now(), now());

insert into department
values ('802bec54-d1f3-11ec-baf3-526e5eed5702', '802be894-d1f3-11ec-baf3-526e5eed5702', '627d0550f40b33cd4a036586',
        'department1', false);

insert into project
values ('802bec5e-d1f3-11ec-baf3-526e5eed5702', '802bec54-d1f3-11ec-baf3-526e5eed5702', '627d0550f40b33cd4a036587',
        'project1', now());

insert into team
values ('802bec68-d1f3-11ec-baf3-526e5eed5702', '802bec5e-d1f3-11ec-baf3-526e5eed5702', '627d0550f40b33cd4a036588',
        'team1');

insert into request_types
values ('802bec86-d1f3-11ec-baf3-526e5eed5702', '802be894-d1f3-11ec-baf3-526e5eed5702', 'vacation request');

insert into request (request_id, company_id, department_id, request_type_id, request_status, user_id, status_changed_by, description, date_since, date_to, date_created)
values ('802be411-d1f3-11ec-baf3-526e5eed5702', '802be894-d1f3-11ec-baf3-526e5eed5702', '802bec54-d1f3-11ec-baf3-526e5eed5702',
        '802bec86-d1f3-11ec-baf3-526e5eed5702', 'in progress', '627d0550f40b33cd4a036585', '', 'asd', now(), now(), now());

insert into positions (position_id, company_id, name)
values ('6406a14c-40a2-4b20-bf27-f385ce3766a2', '802be894-d1f3-11ec-baf3-526e5eed5702', 'golang developer');

insert into grades (grade_id, company_id, name)
values ('c2066439-0a80-4a1c-a7bd-29a13686d0cf', '802be894-d1f3-11ec-baf3-526e5eed5702', 'senior');

insert into vacations (vacation_id, company_id, department_id, request_type_id, user_id, date_since, date_to)
values ('ec5e27da-320a-4c6c-8eed-19dd57237f0b', '802be894-d1f3-11ec-baf3-526e5eed5702', '802bec54-d1f3-11ec-baf3-526e5eed5702',
        '802bec86-d1f3-11ec-baf3-526e5eed5702', '628ac2694165d95f286f6ab4', now(), now());


insert into complaints (complaint_id, company_id, department_id, created_by, data, is_anonymous, date_created)
values ('bd19a8a9-9951-4708-b2a7-ba121317b84b', '802be894-d1f3-11ec-baf3-526e5eed5702', '802bec54-d1f3-11ec-baf3-526e5eed5702',
        '628ac2694165d95f286f6ab4', 'some complaint', false, now());

insert into vacancy (vacancy_id, company_id, title, description, date_created, date_updated)
values ('9b4c6be9-56fe-40fa-b727-ec4a7b48d145', '802be894-d1f3-11ec-baf3-526e5eed5702', 'test title', 'some description', now(), now());

insert into invites (invite_id, company_id, user_email, from_email, description, date_created)
values ('aa496683-0fff-453f-83a8-dddcdd193757', '802be894-d1f3-11ec-baf3-526e5eed5702', 'testuser@gmail.com', 'testhr@gmail.com',
        'test description', now());

insert into task_importance (task_importance_id, company_id, name)
values ('aba9f5ad-967b-4fa6-8eed-36be702d216c', '802be894-d1f3-11ec-baf3-526e5eed5702', 'test name');

insert into task_status (task_status_id, company_id, name)
values ('a575540a-a5b0-4e66-bb62-73f7d647a2cb', '802be894-d1f3-11ec-baf3-526e5eed5702', 'test name');