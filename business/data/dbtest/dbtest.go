// Package dbtest contains supporting code for running tests that hit the DB.
package dbtest

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/data/dbschema"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/foundation/docker"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os/exec"
	"testing"
	"time"
)

// Success and failure markers.
const (
	Success = "\u2713"
	Failed  = "\u2717"

	defaultDBUser = "postgres"
	defaultDBPass = "postgres"
)

// StartDB starts a database instance.
func StartDB() (*docker.Container, error) {
	image := "postgres:14-alpine"
	port := "5432"
	args := []string{"-e", "POSTGRES_PASSWORD=postgres"}

	return docker.StartContainer(image, port, args...)
}

// StopDB stops a running database instance.
func StopDB(c *docker.Container) {
	docker.StopContainer(c.ID)
}

const ShellToUse = "bash"

// Shellout is used to execute commands in shell
func Shellout(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command(ShellToUse, "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}

// NewUnit creates a test database inside a Docker container. It creates the
// required table structure but the database is otherwise empty. It returns
// the database to use as well as a function to call at the end of the test.
func NewUnit(t *testing.T, c *docker.Container, dbName string) (*zap.SugaredLogger, *sqlx.DB, func()) {
	// =========================================================================

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dbM, err := database.Open(database.Config{
		User:       defaultDBUser,
		Password:   defaultDBPass,
		Host:       c.Host,
		Name:       "postgres",
		DisableTLS: true,
	})
	if err != nil {
		t.Fatalf("Opening database connection: %v", err)
	}

	t.Log("Waiting for database to be ready ...")

	if err := database.StatusCheck(ctx, dbM); err != nil {
		t.Fatalf("status check database: %v", err)
	}

	t.Log("Database ready")

	if _, err := dbM.ExecContext(context.Background(), "CREATE DATABASE "+dbName); err != nil {
		t.Fatalf("creating database %s: %v", dbName, err)
	}
	dbM.Close()

	// =========================================================================

	cfg := database.Config{
		User:       defaultDBUser,
		Password:   defaultDBPass,
		Host:       c.Host,
		Name:       dbName,
		DisableTLS: true,
	}
	db, err := database.Open(cfg)
	if err != nil {
		t.Fatalf("Opening database connection: %v", err)
	}

	t.Log("Migrate database ...")

	dir := "../../../migrations"
	dbDSN := fmt.Sprintf("postgres://%s:%s@%s/%s\\?sslmode=disable",
		cfg.User, cfg.Password, cfg.Host, cfg.Name)

	err, _, errout := Shellout(fmt.Sprintf("migrate -path=%s -database=%s up", dir, dbDSN))
	if err != nil {
		t.Fatal(err, errout)
	}

	t.Log("Seed database with example data ...")

	if err = dbschema.Seed(ctx, db); err != nil {
		docker.DumpContainerLogs(t, c.ID)
		t.Fatalf("Seeding error: %s", err)
	}

	t.Log("Ready for testing ...")

	var buf bytes.Buffer
	encoder := zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig())
	writer := bufio.NewWriter(&buf)
	log := zap.New(
		zapcore.NewCore(encoder, zapcore.AddSync(writer), zapcore.DebugLevel)).
		Sugar()

	// teardown is the function that should be invoked when the caller is done
	// with the database.
	teardown := func() {
		t.Helper()
		db.Close()

		log.Sync()

		writer.Flush()
		fmt.Println("******************** LOGS ********************")
		fmt.Print(buf.String())
		fmt.Println("******************** LOGS ********************")
	}

	return log, db, teardown
}

//// Test owns state for running and shutting down tests.
//type Test struct {
//	DB       *sqlx.DB
//	Log      *zap.SugaredLogger
//	Auth     *auth.Auth
//	Teardown func()
//
//	t *testing.T
//}
//
//// NewIntegration creates a database, seeds it, constructs an authenticator.
//func NewIntegration(t *testing.T) *Test {
//	log, db, teardown := NewUnit(t)
//
//	// Create RSA keys to enable authentication in our service.
//	keyID := "4754d86b-7a6d-4df5-9c65-224741361492"
//	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	// Build an authenticator using this private key and id for the key store.
//	auth, err := auth.New(keyID, keystore.NewMap(map[string]*rsa.PrivateKey{keyID: privateKey}))
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	test := Test{
//		DB:       db,
//		Log:      log,
//		Auth:     auth,
//		t:        t,
//		Teardown: teardown,
//	}
//
//	return &test
//}
//
//// Token generates an authenticated token for a user.
//func (test *Test) Token(email, pass string) string {
//	test.t.Log("Generating token for test ...")
//
//	store := dbUser.NewStore(test.Log, test.DB)
//	dbUsr, err := store.QueryByEmail(context.Background(), email)
//	if err != nil {
//		return ""
//	}
//
//	claims := auth.Claims{
//		RegisteredClaims: jwt.RegisteredClaims{
//			Subject:   dbUsr.ID,
//			Issuer:    "service project",
//			ExpiresAt: jwt.NewNumericDate(time.Now().UTC().Add(time.Hour)),
//			IssuedAt:  jwt.NewNumericDate(time.Now().UTC()),
//		},
//		Roles: dbUsr.Roles,
//	}
//
//	token, err := test.Auth.GenerateToken(claims)
//	if err != nil {
//		test.t.Fatal(err)
//	}
//
//	return token
//}

// StringPointer is a helper to get a *string from a string. It is in the tests
// package because we normally don't want to deal with pointers to basic types
// but it's useful in some tests.
func StringPointer(s string) *string {
	return &s
}

// IntPointer is a helper to get a *int from a int. It is in the tests package
// because we normally don't want to deal with pointers to basic types but it's
// useful in some tests.
func IntPointer(i int) *int {
	return &i
}
