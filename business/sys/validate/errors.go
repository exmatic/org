package validate

import (
	"encoding/json"
	"errors"
)

// FieldError is used to indicate an error with a specific request field.
type FieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// FieldErrors represents a collection of field errors.
type FieldErrors []FieldError

// Error implements the error interface.
func (fe FieldErrors) Error() string {
	d, err := json.Marshal(fe)
	if err != nil {
		return err.Error()
	}
	return string(d)
}

// Fields returns the fields that failed validation
func (fe FieldErrors) Fields() map[string]string {
	m := make(map[string]string)
	for _, fld := range fe {
		m[fld.Field] = fld.Error
	}
	return m
}

// IsFieldErrors checks if an error of type FieldErrors exists.
func IsFieldErrors(err error) bool {
	var fe FieldErrors
	return errors.As(err, &fe)
}

// GetFieldErrors returns a copy of the FieldErrors pointer.
func GetFieldErrors(err error) FieldErrors {
	var fe FieldErrors
	if !errors.As(err, &fe) {
		return nil
	}
	return fe
}

// FilterError is used to indicate an error with a specific request field.
type FilterError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// FilterErrors represents a collection of field errors.
type FilterErrors []FilterError

// Error implements the error interface.
func (fe FilterErrors) Error() string {
	d, err := json.Marshal(fe)
	if err != nil {
		return err.Error()
	}
	return string(d)
}

// Fields returns the fields that failed validation
func (fe FilterErrors) Fields() map[string]string {
	m := make(map[string]string)
	for _, fld := range fe {
		m[fld.Field] = fld.Error
	}
	return m
}

func ToFilterErrors(errors []ValidationError) (out FilterErrors) {
	for _, v := range errors {
		out = append(out, FilterError{
			Field: v.Key,
			Error: v.Message,
		})
	}

	return out
}

// IsFieldErrors checks if an error of type FieldErrors exists.
func IsFilterErrors(err error) bool {
	var fe FilterErrors
	return errors.As(err, &fe)
}

// GetFieldErrors returns a copy of the FieldErrors pointer.
func GetFilterErrors(err error) FilterErrors {
	var fe FilterErrors
	if !errors.As(err, &fe) {
		return nil
	}
	return fe
}
