package department

import (
	"gitlab.com/exmatic/org/business/core/department/db"
	"unsafe"
)

type Department struct {
	ID                   string `json:"department_id"`
	CompanyID            string `json:"company_id"`
	DepartmentDirectorID string `json:"department_director_id,omitempty"`
	Name                 string `json:"name"`
	IsProjectless        bool   `json:"is_projectless"`
}

// NewDepartment is what we require from clients when adding a Department.
type NewDepartment struct {
	CompanyID     string `json:"company_id"`
	Name          string `json:"name"`
	IsProjectless bool   `json:"is_projectless"`
}

// UpdateDepartment defines what information may be provided to modify an
// existing Department. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateDepartment struct {
	DepartmentDirectorID *string `json:"department_director_id"`
	Name                 *string `json:"name"`
}

// =============================================================================

func toDepartment(dbx db.Department) Department {
	x := (*Department)(unsafe.Pointer(&dbx))
	return *x
}

func toDepartmentSlice(dbxx []db.Department) []Department {
	xx := make([]Department, len(dbxx))
	for i, x := range dbxx {
		xx[i] = toDepartment(x)
	}
	return xx
}
