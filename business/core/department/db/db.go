// Package db contains Department related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a Department to the database. It returns the created Department with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Department) error {
	const q = `
	INSERT INTO department
		(department_id,  company_id, department_director_id, name, is_projectless)
	VALUES
		(:department_id, :company_id, :department_director_id, :name, :is_projectless)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting Department: %w", err)
	}

	return nil
}

// Update modifies data about a Department. It will error if the specified ID is
// invalid or does not reference an existing Department.
func (s Store) Update(ctx context.Context, x Department) error {
	const q = `
	UPDATE
		department
	SET
		"department_director_id" = :department_director_id,
		"name" = :name
	WHERE
		department_id = :department_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating Department DepartmentID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the Department identified by a given ID.
func (s Store) Delete(ctx context.Context, DepartmentID string) error {
	data := struct {
		DepartmentID string `db:"department_id"`
	}{
		DepartmentID: DepartmentID,
	}

	const q = `
	DELETE FROM
		department
	WHERE
		department_id = :department_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting Department DepartmentID[%v]: %w", DepartmentID, err)
	}

	return nil
}

// Query gets all Department from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Department, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM department
	WHERE
		company_id = :company_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Department
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting Department: %w", err)
	}

	return xx, nil
}

// QueryByID finds the Department identified by a given ID.
func (s Store) QueryByID(ctx context.Context, DepartmentID string) (Department, error) {
	data := struct {
		DepartmentID string `db:"department_id"`
	}{
		DepartmentID: DepartmentID,
	}

	const q = `
	SELECT * FROM department
	WHERE
		department_id = :department_id`

	var x Department
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Department{}, fmt.Errorf("selecting Department DepartmentID[%v]: %w", DepartmentID, err)
	}

	return x, nil
}
