package db

type Department struct {
	ID                   string `db:"department_id"`
	CompanyID            string `db:"company_id"`
	DepartmentDirectorID string `db:"department_director_id"`
	Name                 string `db:"name"`
	IsProjectless        bool   `db:"is_projectless"`
}
