// Package department provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package department

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/department/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Department access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Department api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Department to the database. It returns the created Department with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewDepartment) (Department, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Department{}, err
	}

	dbx := db.Department{
		ID:            validate.GenerateID(),
		CompanyID:     usr.Info.CompanyID,
		Name:          x.Name,
		IsProjectless: x.IsProjectless,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Department{}, fmt.Errorf("create: %w", err)
	}

	return toDepartment(dbx), nil
}

// Update modifies data about a Department. It will error if the specified ID is
// invalid or does not reference an existing Department.
func (c Core) Update(ctx context.Context, DepartmentID string, up UpdateDepartment) error {
	if err := validate.CheckID(DepartmentID); err != nil {
		return core2.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, DepartmentID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core2.ErrNotFound
		}
		return fmt.Errorf("updating Department DepartmentID[%v]: %w", DepartmentID, err)
	}

	if up.DepartmentDirectorID != nil {
		dbx.DepartmentDirectorID = *up.DepartmentDirectorID
	}
	if up.Name != nil {
		dbx.Name = *up.Name
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Department identified by a given ID.
func (c Core) Delete(ctx context.Context, DepartmentID string) error {
	if err := validate.CheckID(DepartmentID); err != nil {
		return core2.ErrInvalidID
	}

	if err := c.store.Delete(ctx, DepartmentID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Departments from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Department, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core2.ErrInvalidID
	}
	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toDepartmentSlice(dbXX), nil
}

// QueryByID finds the Department identified by a given ID.
func (c Core) QueryByID(ctx context.Context, DepartmentID string) (Department, error) {
	if err := validate.CheckID(DepartmentID); err != nil {
		return Department{}, core2.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, DepartmentID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Department{}, core2.ErrNotFound
		}
		return Department{}, fmt.Errorf("query: %w", err)
	}

	return toDepartment(dbx), nil
}
