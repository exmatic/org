package user

import (
	"context"
	"gitlab.com/exmatic/org/foundation/web"
	"time"

	"gitlab.com/exmatic/org/business/core/role"
)

type User struct {
	ID        string    `json:"id"`
	Profile   Profile   `json:"profile"`
	Info      *Info     `json:"info"`
	Role      role.Role `json:"role"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type Profile struct {
	Email     string    `json:"email" bson:"email"`
	Username  string    `json:"username" bson:"username"`
	Firstname string    `json:"firstname" bson:"firstname"`
	Lastname  string    `json:"lastname" bson:"lastname"`
	BirthDate time.Time `json:"birthDate" bson:"birthDate"`
	Password  string    `json:"password" bson:"password"`
}

type UpdateUserInfo struct {
	CompanyID          *string    `json:"company_id,omitempty"`
	DepartmentID       *string    `json:"department_id,omitempty"`
	ProjectID          *string    `json:"project_id,omitempty"`
	TeamID             *string    `json:"team_id,omitempty"`
	PositionID         *string    `json:"position_id,omitempty"`
	GradeID            *string    `json:"grade_id,omitempty"`
	Salary             *int       `json:"salary,omitempty"`
	ActiveVacationDays *int       `json:"active_vacation_days,omitempty"`
	IsActive           *int       `json:"is_active,omitempty"`
	Skills             []string   `json:"skills,omitempty"`
	Responsibilities   []string   `json:"responsibilities,omitempty"`
	WorkingSince       *time.Time `json:"date_working_since,omitempty"`
	Role               *role.Role `json:"role,omitempty"`
}

type Info struct {
	CompanyID          string    `json:"company_id"`
	DepartmentID       string    `json:"department_id"`
	ProjectID          string    `json:"project_id"`
	TeamID             string    `json:"team_id"`
	PositionID         string    `json:"position_id"`
	GradeID            string    `json:"grade_id"`
	Salary             int       `json:"salary"`
	ActiveVacationDays int       `json:"active_vacation_days"`
	IsActive           int       `json:"is_active"`
	Skills             []string  `json:"skills"`
	Responsibilities   []string  `json:"responsibilities"`
	WorkingSince       time.Time `json:"date_working_since"`
}

func UserFromContext(ctx context.Context) (*User, error) {
	usr := ctx.Value("user").(*User)
	if usr == nil {
		return nil, web.NewShutdownError("no user data in ctx")
	}

	return usr, nil
}
