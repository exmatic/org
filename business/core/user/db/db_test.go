package db

//
//import (
//	"context"
//	"net/http"
//	"testing"
//	"time"
//
//	"github.com/google/uuid"
//
//	user2 "gitlab.com/exmatic/org/business/core/user"
//)
//
//func TestStore_FetchByID(t *testing.T) {
//	ctx := context.Background()
//
//	access := "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwicm9sZSI6IlJPTEVfQURNSU4iLCJleHAiOjE2NTMxNDkwMzB9.HtuOYCJBmbnxUlFHb6yZxDbDyAX4OztbYvs_y9Yi9RM"
//	refresh := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwiZXhwIjoxNjU1NzQwMTMwfQ.m7XZ5jXRkvmTm-6tV-j43pkZCJpI9_gDC_IIY9Z2CCM"
//
//	ctx = context.WithValue(ctx, "tokens", []string{access, refresh})
//
//	store := NewStore(&http.Client{})
//
//	pair, user, err := store.FetchByID(ctx, "62878d358c1879c0a04690f2")
//	if err != nil {
//		t.Fatalf("err : %v", err)
//	}
//
//	t.Log(pair)
//	t.Log(*user)
//
//}
//
//func TestStore_VerifyTokens(t *testing.T) {
//	ctx := context.Background()
//
//	access := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwicm9sZSI6IlJPTEVfQURNSU4iLCJleHAiOjE2NTMxNTA1NTJ9.JExknqK8KHKxQnb2FFMRL2I_BDiAQ0fmVthZUl-n-dA"
//	refresh := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwiZXhwIjoxNjU1NzQxNjUyfQ.pVe9dFHWAwHVor9n9K0L5h_0P6boxUQXy0alAmYBCgc"
//
//	store := NewStore(&http.Client{})
//
//	tokens, user, err := store.VerifyTokens(ctx, []string{access, refresh})
//	if err != nil {
//		t.Fatalf("err : %v", err)
//	}
//
//	t.Log(tokens)
//	t.Log(*user)
//}
//
//func TestStore_UpdateUserInfo(t *testing.T) {
//	ctx := context.Background()
//
//	access := "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwicm9sZSI6IlJPTEVfQURNSU4iLCJleHAiOjE2NTMxNTI4NjV9.IAoEE0xJlo6ABDWshtCk4nZETKqEsadd2KfK7NvBQRw"
//	refresh := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4NzhkMzU4YzE4NzljMGEwNDY5MGYyIiwiZXhwIjoxNjU1NzQzOTY1fQ.zUqcCN-xy7df18mBO9kLE6PF-PGDolYk9gJ6zVRlkP8"
//
//	ctx = context.WithValue(ctx, "tokens", []string{access, refresh})
//
//	store := NewStore(&http.Client{})
//
//	pair, err := store.UpdateUserInfo(ctx, &user2.Info{
//		CompanyID:          uuid.New().String(),
//		DepartmentID:       uuid.New().String(),
//		ProjectID:          uuid.New().String(),
//		TeamID:             uuid.New().String(),
//		PositionID:         uuid.New().String(),
//		GradeID:            uuid.New().String(),
//		Salary:             500000,
//		ActiveVacationDays: 30,
//		IsActive:           10,
//		Skills:             []string{"SUPER", "COOL", "THE", "BEST"},
//		Responsibilities:   []string{"AYBAR", "IS", "OR", "ALTERCOLT"},
//		WorkingSince:       time.Now(),
//	},
//		"62878d358c1879c0a04690f2")
//	if err != nil {
//		t.Fatalf("err : %v", err)
//	}
//
//	t.Log(pair)
//}
