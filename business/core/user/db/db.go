package db

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/org/business/core/user"
)

//TODO : EXPORT ADDR:HOST
var (
	url = "http://localhost:8080/v1"
)

var (
	ErrNoTokensInContext = errors.New("no tokens in context")
)

type AuthStore struct {
	db *http.Client
}

func NewStore(db *http.Client) *AuthStore {
	return &AuthStore{
		db: db,
	}
}

func (s *AuthStore) UpdateUserInfo(ctx context.Context, usr *user.UpdateUserInfo, id string) error {
	endpoint := url + "/user/info/" + id
	if usr == nil {
		return errors.New("no user data")
	}

	data, err := json.Marshal(usr)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}

	body := bytes.NewReader(data)
	req, err := http.NewRequestWithContext(ctx, http.MethodPut, endpoint, body)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}

	token, err := tokenFromContext(ctx)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := s.db.Do(req)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}
	defer resp.Body.Close()

	res := struct {
		Error string `json:"error"`
	}{}

	if err = json.NewDecoder(resp.Body).Decode(&res); err != nil {
		return fmt.Errorf("updating user info init : decoding error %v", err)
	}

	if res.Error != "" {
		return fmt.Errorf("updating user info init : %v", res.Error)
	}

	return nil
}

func (s *AuthStore) UpdateUserInfoInit(ctx context.Context, usr *user.UpdateUserInfo, id string) error {
	endpoint := url + "/user/info/init/" + id
	if usr == nil {
		return errors.New("no user data")
	}

	data, err := json.Marshal(usr)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}

	body := bytes.NewReader(data)
	req, err := http.NewRequestWithContext(ctx, http.MethodPut, endpoint, body)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}

	token, err := tokenFromContext(ctx)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := s.db.Do(req)
	if err != nil {
		return fmt.Errorf("updating user info : %w", err)
	}
	defer resp.Body.Close()

	res := struct {
		Error string `json:"error"`
	}{}

	if err = json.NewDecoder(resp.Body).Decode(&res); err != nil {
		return fmt.Errorf("updating user info init : decoding error %v", err)
	}

	if res.Error != "" {
		return fmt.Errorf("updating user info init : %v", res.Error)
	}

	return nil
}

func (s *AuthStore) FetchByID(ctx context.Context, id string) (*user.User, error) {
	if id == "" {
		return nil, errors.New("id should not be empty")
	}
	endpoint := url + "/user/" + id

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}

	token, err := tokenFromContext(ctx)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := s.db.Do(req)
	if err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}
	defer resp.Body.Close()

	res := struct {
		Error string `json:"error"`
		User  user.User
	}{}

	if err = json.NewDecoder(resp.Body).Decode(&res); err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}

	if res.Error != "" {
		return nil, fmt.Errorf("FetchByID : %v", res.Error)
	}

	return &res.User, nil
}

func (s *AuthStore) VerifyTokens(ctx context.Context, token string) (*user.User, error) {
	endpoint := url + "/verify"
	reqData := struct {
		AccessToken string `json:"token"`
	}{
		AccessToken: token,
	}
	data, err := json.Marshal(reqData)
	if err != nil {
		return nil, fmt.Errorf("verifiying tokens : %w", err)
	}

	body := bytes.NewReader(data)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint, body)
	if err != nil {
		return nil, fmt.Errorf("verifying tokens : %w", err)
	}

	resp, err := s.db.Do(req)
	if err != nil {
		return nil, fmt.Errorf("verifying tokens : %w", err)
	}
	defer resp.Body.Close()

	//res := struct {
	//	Error string `json:"error"`
	//	User  user.User
	//}{}

	usr := user.User{}

	if err = json.NewDecoder(resp.Body).Decode(&usr); err != nil {
		return nil, fmt.Errorf("decoding : %w", err)
	}

	//if res.Error != "" {
	//	return nil, fmt.Errorf("verifying tokens : %w", errors.New(res.Error))
	//}

	return &usr, nil
}

func tokenFromContext(ctx context.Context) (string, error) {
	token := ctx.Value("token").(*string)
	if token == nil {
		return "", ErrNoTokensInContext
	}

	return *token, nil
}
