package task_test

import (
	"context"
	"errors"
	"fmt"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/task"
	reqdb "gitlab.com/exmatic/org/business/core/task/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/exmatic/org/business/data/dbtest"
)

var c *docker.Container

func Test_Task(t *testing.T) {
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	//mailer := mailer.New("smtp.mailtrap.io", 2525, "2985b09edba520", "302520886b0aa1", "Exmatic[No Reply] <no-reply@inbox.mailtrap.io>")
	//authStore := userdb.NewStore(&http.Client{})

	core := task.NewCore(log, db)

	t.Log("Given the need to work with Task records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Task.", testID)
		{
			ctx := context.WithValue(context.Background(), "user", &user.User{
				ID:        "628a47ac1b19204b0e831722",
				Profile:   user.Profile{Email: "asdtest@gmail.com"},
				Info:      &user.Info{CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702"},
				Role:      "ROLE_ADMIN",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			})

			now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

			np := task.NewTask{
				CompanyID:        "802be894-d1f3-11ec-baf3-526e5eed5702",
				DepartmentID:     "802bec54-d1f3-11ec-baf3-526e5eed5702",
				ProjectID:        "802bec5e-d1f3-11ec-baf3-526e5eed5702",
				TeamID:           "802bec68-d1f3-11ec-baf3-526e5eed5702",
				AssignedBy:       "628a47ac1b19204b0e831722",
				AssignedTo:       "628a47ac1b19204b0e831725",
				Title:            "test title",
				Description:      "test description",
				TaskImportanceID: "aba9f5ad-967b-4fa6-8eed-36be702d216c",
				TaskStatusID:     "a575540a-a5b0-4e66-bb62-73f7d647a2cb",
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Task.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Task by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Task by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Task.\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Task.", dbtest.Success, testID)

			upd := task.UpdateTask{
				Description: dbtest.StringPointer("updated test description"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update Task.", dbtest.Success, testID)

			Tasks, err := core.Query(ctx, np.CompanyID, filter.Filters{
				PageNumber:   1,
				RowsPerPage:  3,
				Sort:         "date_created",
				SortSafeList: []string{"date_created", "-date_created"},
			}, reqdb.FilterTask{})
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Task.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original Task
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Description = *upd.Description

			var idx int
			for i, p := range Tasks {
				if p.ID == want.ID {
					idx = i
				}
			}

			if diff := cmp.Diff(want, Tasks[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Task. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Task.", dbtest.Success, testID)

			saved, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Task.", dbtest.Success, testID)

			if saved.Description != *upd.Description {
				t.Fatalf("\t%s\tTest %d:\tShould be able to see updated TaskStatus field : got %q want %q.", dbtest.Failed, testID, saved.Description, *upd.Description)
			} else {
				t.Logf("\t%s\tTest %d:\tShould be able to see updated TaskStatus field.", dbtest.Success, testID)
			}

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Task.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Task : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Task.", dbtest.Success, testID)
		}
	}
}
