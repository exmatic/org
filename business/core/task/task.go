// Package task provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package task

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/filter"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/task/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Task access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Task api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Task to the database. It returns the created Task with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewTask, now time.Time) (Task, error) {

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Task{}, err
	}

	dbx := db.Task{
		ID:               validate.GenerateID(),
		CompanyID:        usr.Info.CompanyID,
		DepartmentID:     x.DepartmentID,
		ProjectID:        x.ProjectID,
		TeamID:           x.TeamID,
		AssignedBy:       usr.ID,
		AssignedTo:       x.AssignedTo,
		Title:            x.Title,
		Description:      x.Description,
		TaskImportanceID: x.TaskImportanceID,
		TaskStatusID:     x.TaskStatusID,
		DateCreated:      now,
		DateUpdated:      now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Task{}, fmt.Errorf("create: %w", err)
	}

	return toTask(dbx), nil
}

// Update modifies data about a Task. It will error if the specified ID is
// invalid or does not reference an existing Task.
func (c Core) Update(ctx context.Context, TaskID string, up UpdateTask) error {
	if err := validate.CheckID(TaskID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Task TaskID[%v]: %w", TaskID, err)
	}

	if up.AssignedTo != nil {
		dbx.AssignedTo = *up.AssignedTo
	}
	if up.Title != nil {
		dbx.Title = *up.Title
	}
	if up.Description != nil {
		dbx.Description = *up.Description
	}
	if up.TaskImportanceID != nil {
		dbx.TaskImportanceID = *up.TaskImportanceID
	}
	if up.TaskStatusID != nil {
		dbx.TaskStatusID = *up.TaskStatusID
	}
	dbx.DateUpdated = time.Now()

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Task identified by a given ID.
func (c Core) Delete(ctx context.Context, TaskID string) error {
	if err := validate.CheckID(TaskID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, TaskID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Tasks from the database.
func (c Core) Query(ctx context.Context, companyID string, filters filter.Filters, filterTask db.FilterTask) ([]Task, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	// Initialize a new Validator instance.
	v := validate.New()

	if db.ValidateFilterTask(v, filterTask); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	if filter.ValidateFilters(v, filters); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	dbXX, err := c.store.Query(ctx, companyID, filters, filterTask)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toTaskSlice(dbXX), nil
}

// QueryByID finds the Task identified by a given ID.
func (c Core) QueryByID(ctx context.Context, TaskID string) (Task, error) {
	if err := validate.CheckID(TaskID); err != nil {
		return Task{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Task{}, core.ErrNotFound
		}
		return Task{}, fmt.Errorf("query: %w", err)
	}

	return toTask(dbx), nil
}
