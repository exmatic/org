package db

import (
	"gitlab.com/exmatic/org/business/sys/validate"
	"time"
)

type Task struct {
	ID               string    `db:"task_id"`
	CompanyID        string    `db:"company_id"`
	DepartmentID     string    `db:"department_id"`
	ProjectID        string    `db:"project_id"`
	TeamID           string    `db:"team_id"`
	AssignedBy       string    `db:"assigned_by"`
	AssignedTo       string    `db:"assigned_to"`
	Title            string    `db:"title"`
	Description      string    `db:"description"`
	TaskImportanceID string    `db:"task_importance_id"`
	TaskStatusID     string    `db:"task_status_id"`
	DateCreated      time.Time `db:"date_created"`
	DateUpdated      time.Time `db:"date_updated"`
}

type FilterTask struct {
	DepartmentID     string `db:"department_id"`
	ProjectID        string `db:"project_id"`
	TeamID           string `db:"team_id"`
	AssignedBy       string `db:"assigned_by"`
	AssignedTo       string `db:"assigned_to"`
	TaskImportanceID string `db:"task_importance_id"`
	TaskStatusID     string `db:"task_status_id"`
}

func ValidateFilterTask(v *validate.Validator, x FilterTask) {
	//if x.DepartmentID != "" {
	//	if err := validate.CheckID(x.DepartmentID); err != nil {
	//		v.Check(false, "department_id", err.Error())
	//	}
	//}
	//
}
