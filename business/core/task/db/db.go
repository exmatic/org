// Package db contains Task related CRUD functionality.
package db

import (
	"context"
	"fmt"
	"gitlab.com/exmatic/org/business/sys/filter"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	Log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		Log: log,
		db:  db,
	}
}

// Create adds a Task to the database. It returns the created Task with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Task) error {
	const q = `
	INSERT INTO tasks
		(task_id, company_id, department_id, project_id, team_id, assigned_by, assigned_to, title, description, task_importance_id, task_status_id, date_created, date_updated)
	VALUES
		(:task_id, :company_id, :department_id, :project_id, :team_id, :assigned_by, :assigned_to, :title, :description, :task_importance_id, :task_status_id, :date_created, :date_updated)`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting Task: %w", err)
	}

	return nil
}

// Update modifies data about a Task. It will error if the specified ID is
// invalid or does not reference an existing Task.
func (s Store) Update(ctx context.Context, x Task) error {
	const q = `
	UPDATE
		tasks
	SET
		"assigned_to" = :assigned_to,
		"title" = :title,
		"description" = :description,
		"task_importance_id" = :task_importance_id,
		"task_status_id" = :task_status_id
	WHERE
		task_id = :task_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("updating Task TaskID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the Task identified by a given ID.
func (s Store) Delete(ctx context.Context, TaskID string) error {
	data := struct {
		TaskID string `db:"task_id"`
	}{
		TaskID: TaskID,
	}

	const q = `
	DELETE FROM
		tasks
	WHERE
		task_id = :task_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting Task TaskID[%v]: %w", TaskID, err)
	}

	return nil
}

// Query gets all Task from the database.
func (s Store) Query(ctx context.Context, companyID string, filters filter.Filters, filterTask FilterTask) ([]Task, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		FilterTask
	}{
		CompanyID:   companyID,
		Offset:      (filters.PageNumber - 1) * filters.RowsPerPage,
		RowsPerPage: filters.RowsPerPage,
		FilterTask:  filterTask,
	}

	q := fmt.Sprintf(`
	SELECT
		*
	FROM 
		tasks
	WHERE
		(company_id = :company_id) AND
		(
			(:department_id = '' OR CAST(department_id as text) = :department_id) AND
			(:project_id = '' OR CAST(project_id as text) = :project_id) AND
			(:team_id = '' OR CAST(team_id as text) = :team_id) AND
			(:assigned_by = '' OR CAST(assigned_by as text) = :assigned_by) AND
			(:assigned_to = '' OR CAST(assigned_to as text) = :assigned_to) AND
			(:task_importance_id = '' OR CAST(task_importance_id as text) = :task_importance_id) AND
			(:task_status_id = '' OR CAST(task_status_id as text) = :task_status_id)
		)
	ORDER BY
		%s %s
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`,
		filters.SortColumn(), filters.SortDirection())

	var xx []Task
	if err := database.NamedQuerySlice(ctx, s.Log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting Task: %w", err)
	}

	return xx, nil
}

// QueryByID finds the Task identified by a given ID.
func (s Store) QueryByID(ctx context.Context, TaskID string) (Task, error) {
	data := struct {
		TaskID string `db:"task_id"`
	}{
		TaskID: TaskID,
	}

	const q = `
	SELECT 
		*
	FROM 
		tasks
	WHERE 
		task_id = :task_id`

	var x Task
	if err := database.NamedQueryStruct(ctx, s.Log, s.db, q, data, &x); err != nil {
		return Task{}, fmt.Errorf("selecting Task TaskID[%v]: %w", TaskID, err)
	}

	return x, nil
}
