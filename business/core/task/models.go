package task

import (
	"gitlab.com/exmatic/org/business/core/task/db"
	"time"
)

type Task struct {
	ID               string    `json:"task_id"`
	CompanyID        string    `json:"company_id"`
	DepartmentID     string    `json:"department_id"`
	ProjectID        string    `json:"project_id"`
	TeamID           string    `json:"team_id"`
	AssignedBy       string    `json:"assigned_by"`
	AssignedTo       string    `json:"assigned_to"`
	Title            string    `json:"title"`
	Description      string    `json:"description"`
	TaskImportanceID string    `json:"task_importance_id"`
	TaskStatusID     string    `json:"task_status_id"`
	DateCreated      time.Time `json:"date_created"`
	DateUpdated      time.Time `json:"date_updated"`
}

// NewTask is what we require from clients when adding a Task.
type NewTask struct {
	CompanyID        string `json:"company_id"`
	DepartmentID     string `json:"department_id"`
	ProjectID        string `json:"project_id"`
	TeamID           string `json:"team_id"`
	AssignedBy       string `json:"assigned_by"`
	AssignedTo       string `json:"assigned_to"`
	Title            string `json:"title"`
	Description      string `json:"description"`
	TaskImportanceID string `json:"task_importance_id"`
	TaskStatusID     string `json:"task_status_id"`
}

// UpdateTask defines what information may be provided to modify an
// existing Task. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateTask struct {
	AssignedTo       *string `json:"assigned_to"`
	Title            *string `json:"title"`
	Description      *string `json:"description"`
	TaskImportanceID *string `json:"task_importance_id"`
	TaskStatusID     *string `json:"task_status_id"`
}

// =============================================================================

func toTask(dbx db.Task) Task {
	x := Task{
		ID:               dbx.ID,
		CompanyID:        dbx.CompanyID,
		DepartmentID:     dbx.DepartmentID,
		ProjectID:        dbx.ProjectID,
		TeamID:           dbx.TeamID,
		AssignedBy:       dbx.AssignedBy,
		AssignedTo:       dbx.AssignedTo,
		Title:            dbx.Title,
		Description:      dbx.Description,
		TaskImportanceID: dbx.TaskImportanceID,
		TaskStatusID:     dbx.TaskStatusID,
		DateCreated:      dbx.DateCreated,
		DateUpdated:      dbx.DateUpdated,
	}

	return x
}

func toTaskSlice(dbXX []db.Task) []Task {
	xx := make([]Task, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toTask(dbx)
	}
	return xx
}
