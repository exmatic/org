package db

import "time"

type Company struct {
	ID             string    `db:"company_id"`
	OwnerID        string    `db:"owner_id"`
	Name           string    `db:"name"`
	Phone          string    `db:"phone"`
	Email          string    `db:"email"`
	Address        string    `db:"address"`
	BIK            string    `db:"bik"`
	Website        string    `db:"website"`
	DateCreated    time.Time `db:"date_created"`
	DateRegistered time.Time `db:"date_registered"`
}
