// Package db contains company related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	Log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		Log: log,
		db:  db,
	}
}

// Create adds a company to the database. It returns the created company with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Company) error {
	const q = `
	INSERT INTO company
		(company_id, owner_id, name, phone, email, address, bik, website, date_created, date_registered)
	VALUES
		(:company_id, :owner_id, :name, :phone, :email, :address, :bik, :website, :date_created, :date_registered)`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting company: %w", err)
	}

	return nil
}

// Update modifies data about a company. It will error if the specified ID is
// invalid or does not reference an existing company.
func (s Store) Update(ctx context.Context, x Company) error {
	const q = `
	UPDATE
		company
	SET
		"owner_id" = :owner_id,
		"name" = :name,
		"phone" = :phone,
		"email" = :email,
		"address" = :address,
		"bik" = :bik,
		"website" = :website
	WHERE
		company_id = :company_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("updating company companyID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the company identified by a given ID.
func (s Store) Delete(ctx context.Context, companyID string) error {
	data := struct {
		CompanyID string `db:"company_id"`
	}{
		CompanyID: companyID,
	}

	const q = `
	DELETE FROM
		company
	WHERE
		company_id = :company_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting company companyID[%v]: %w", companyID, err)
	}

	return nil
}

// Query gets all company from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]Company, error) {
	data := struct {
		Offset      int `db:"offset"`
		RowsPerPage int `db:"rows_per_page"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM company
	ORDER BY
		date_registered
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Company
	if err := database.NamedQuerySlice(ctx, s.Log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting company: %w", err)
	}

	return xx, nil
}

// QueryByID finds the company identified by a given ID.
func (s Store) QueryByID(ctx context.Context, companyID string) (Company, error) {
	data := struct {
		CompanyID string `db:"company_id"`
	}{
		CompanyID: companyID,
	}

	const q = `
	SELECT * FROM company
	WHERE
		company_id = :company_id`

	var x Company
	if err := database.NamedQueryStruct(ctx, s.Log, s.db, q, data, &x); err != nil {
		return Company{}, fmt.Errorf("selecting company companyID[%v]: %w", companyID, err)
	}

	return x, nil
}
