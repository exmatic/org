package company

import (
	"gitlab.com/exmatic/org/business/core/company/db"
	"time"
	"unsafe"
)

type Company struct {
	ID             string    `json:"id"`
	OwnerID        string    `json:"owner_id"`
	Name           string    `json:"name"`
	Phone          string    `json:"phone"`
	Email          string    `json:"email"`
	Address        string    `json:"address,omitempty"`
	BIK            string    `json:"bik,omitempty"`
	Website        string    `json:"website,omitempty"`
	DateCreated    time.Time `json:"date_created"`
	DateRegistered time.Time `json:"date_registered"`
}

// NewCompany is what we require from clients when adding a Company.
type NewCompany struct {
	Name        string    `json:"name"`
	Phone       string    `json:"phone"`
	Email       string    `json:"email"`
	Address     string    `json:"address,omitempty"`
	BIK         string    `json:"bik,omitempty"`
	Website     string    `json:"website,omitempty"`
	DateCreated time.Time `json:"date_created"`
}

// UpdateCompany defines what information may be provided to modify an
// existing Company. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateCompany struct {
	OwnerID *string `json:"owner_id"`
	Name    *string `json:"name"`
	Phone   *string `json:"phone"`
	Email   *string `json:"email"`
	Address *string `json:"address,omitempty"`
	BIK     *string `json:"bik,omitempty"`
	Website *string `json:"website,omitempty"`
}

// =============================================================================

func toCompany(dbx db.Company) Company {
	x := (*Company)(unsafe.Pointer(&dbx))
	return *x
}

func toCompanySlice(dbXX []db.Company) []Company {
	xx := make([]Company, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toCompany(dbx)
	}
	return xx
}
