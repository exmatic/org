// Package company provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package company

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/role"
	"gitlab.com/exmatic/org/business/core/user"
	userdb "gitlab.com/exmatic/org/business/core/user/db"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/company/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Company access.
type Core struct {
	store     db.Store
	authStore *userdb.AuthStore
}

// NewCore constructs a core for Company api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB, store *userdb.AuthStore) Core {
	return Core{
		store:     db.NewStore(log, sqlxDB),
		authStore: store,
	}
}

// Create adds a Company to the database. It returns the created Company with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewCompany, now time.Time) (Company, error) {

	dbx := db.Company{
		ID:             validate.GenerateID(),
		Name:           x.Name,
		Phone:          x.Phone,
		Email:          x.Email,
		Address:        x.Address,
		BIK:            x.BIK,
		Website:        x.Website,
		DateCreated:    x.DateCreated,
		DateRegistered: now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Company{}, fmt.Errorf("create: %w", err)
	}

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Company{}, err
	}

	vacDays := 0

	ux := user.UpdateUserInfo{
		CompanyID:          &dbx.ID,
		ActiveVacationDays: &vacDays,
		WorkingSince:       &now,
		Role:               &role.Admin,
	}

	err = c.authStore.UpdateUserInfoInit(ctx, &ux, usr.ID)
	if err != nil {
		return Company{}, err
	}

	// TODO(docs) create company signature

	return toCompany(dbx), nil
}

// Update modifies data about a Company. It will error if the specified ID is
// invalid or does not reference an existing Company.
func (c Core) Update(ctx context.Context, CompanyID string, up UpdateCompany) error {
	if err := validate.CheckID(CompanyID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, CompanyID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Company CompanyID[%v]: %w", CompanyID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}
	if up.Email != nil {
		dbx.Email = *up.Email
	}
	if up.BIK != nil {
		dbx.BIK = *up.BIK
	}
	if up.Address != nil {
		dbx.Address = *up.Address
	}
	if up.Phone != nil {
		dbx.Phone = *up.Phone
	}
	if up.OwnerID != nil {
		dbx.OwnerID = *up.OwnerID
	}
	if up.Website != nil {
		dbx.Website = *up.Website
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Company identified by a given ID.
func (c Core) Delete(ctx context.Context, CompanyID string) error {
	if err := validate.CheckID(CompanyID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, CompanyID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Companys from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]Company, error) {
	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toCompanySlice(dbXX), nil
}

// QueryByID finds the Company identified by a given ID.
func (c Core) QueryByID(ctx context.Context, CompanyID string) (Company, error) {
	if err := validate.CheckID(CompanyID); err != nil {
		return Company{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, CompanyID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Company{}, core.ErrNotFound
		}
		return Company{}, fmt.Errorf("query: %w", err)
	}

	return toCompany(dbx), nil
}
