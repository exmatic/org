// Package db contains team related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a team to the database. It returns the created team with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Team) error {
	const q = `
	INSERT INTO team
		(team_id, project_id, team_lead_id, name)
	VALUES
		(:team_id, :project_id, :team_lead_id, :name)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting team: %w", err)
	}

	return nil
}

// Update modifies data about a team. It will error if the specified ID is
// invalid or does not reference an existing team.
func (s Store) Update(ctx context.Context, x Team) error {
	const q = `
	UPDATE
		team
	SET
		"team_lead_id" = :team_lead_id,
		"name" = :name
	WHERE
		team_id = :team_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating team teamID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the team identified by a given ID.
func (s Store) Delete(ctx context.Context, teamID string) error {
	data := struct {
		TeamID string `db:"team_id"`
	}{
		TeamID: teamID,
	}

	const q = `
	DELETE FROM
		team
	WHERE
		team_id = :team_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting team teamID[%v]: %w", teamID, err)
	}

	return nil
}

// Query gets all team from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, projectID string) ([]Team, error) {
	data := struct {
		ProjectID   string `db:"project_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		ProjectID:   projectID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM team
	WHERE
		project_id = :project_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Team
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting team: %w", err)
	}

	return xx, nil
}

// QueryByID finds the team identified by a given ID.
func (s Store) QueryByID(ctx context.Context, teamID string) (Team, error) {
	data := struct {
		TeamID string `db:"team_id"`
	}{
		TeamID: teamID,
	}

	const q = `
	SELECT * FROM team
	WHERE
		team_id = :team_id`

	var x Team
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Team{}, fmt.Errorf("selecting team teamID[%v]: %w", teamID, err)
	}

	return x, nil
}
