package db

type Team struct {
	ID         string `db:"team_id"`
	ProjectID  string `db:"project_id"`
	TeamLeadID string `db:"team_lead_id"`
	Name       string `db:"name"`
}
