package team_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/team"
	"gitlab.com/exmatic/org/business/data/dbtest"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
)

var c *docker.Container

func Test_team(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := team.NewCore(log, db)

	t.Log("Given the need to work with team records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single team.", testID)
		{
			ctx := context.Background()

			np := team.NewTeam{
				ProjectID: "802bec5e-d1f3-11ec-baf3-526e5eed5702", // from seed
				Name:      "testTeam",
			}

			x, err := core.Create(ctx, np)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a team.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve team by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve team by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same team. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same team.", dbtest.Success, testID)

			upd := team.UpdateTeam{
				Name: dbtest.StringPointer("bts-digital"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update team.", dbtest.Success, testID)

			teams, err := core.Query(ctx, 1, 3, np.ProjectID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated team.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original team
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Name = *upd.Name

			var idx int
			for i, p := range teams {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, teams[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same team. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same team.", dbtest.Success, testID)

			upd = team.UpdateTeam{
				Name: dbtest.StringPointer("Graphic Novels"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update just some fields of team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update just some fields of team.", dbtest.Success, testID)

			saved, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated team.", dbtest.Success, testID)

			if saved.Name != *upd.Name {
				t.Fatalf("\t%s\tTest %d:\tShould be able to see updated Name field : got %q want %q.", dbtest.Failed, testID, saved.Name, *upd.Name)
			} else {
				t.Logf("\t%s\tTest %d:\tShould be able to see updated Name field.", dbtest.Success, testID)
			}

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete team.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted team : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted team.", dbtest.Success, testID)
		}
	}
}
