// Package team provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package team

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/team/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Team access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Team api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Team to the database. It returns the created Team with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewTeam) (Team, error) {
	dbx := db.Team{
		ID:        validate.GenerateID(),
		ProjectID: x.ProjectID,
		Name:      x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Team{}, fmt.Errorf("create: %w", err)
	}

	return toTeam(dbx), nil
}

// Update modifies data about a Team. It will error if the specified ID is
// invalid or does not reference an existing Team.
func (c Core) Update(ctx context.Context, TeamID string, up UpdateTeam) error {
	if err := validate.CheckID(TeamID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TeamID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Invite TeamID[%v]: %w", TeamID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}
	if up.TeamLeadID != nil {
		dbx.TeamLeadID = *up.TeamLeadID
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Team identified by a given ID.
func (c Core) Delete(ctx context.Context, TeamID string) error {
	if err := validate.CheckID(TeamID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, TeamID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Teams from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, projectID string) ([]Team, error) {
	if err := validate.CheckID(projectID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, projectID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toTeamSlice(dbXX), nil
}

// QueryByID finds the Team identified by a given ID.
func (c Core) QueryByID(ctx context.Context, TeamID string) (Team, error) {
	if err := validate.CheckID(TeamID); err != nil {
		return Team{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TeamID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Team{}, core.ErrNotFound
		}
		return Team{}, fmt.Errorf("query: %w", err)
	}

	return toTeam(dbx), nil
}
