package team

import (
	"gitlab.com/exmatic/org/business/core/team/db"
	"unsafe"
)

type Team struct {
	ID         string `json:"team_id"`
	ProjectID  string `json:"project_id"`
	TeamLeadID string `json:"team_lead_id,omitempty"`
	Name       string `json:"name"`
}

// NewTeam is what we require from clients when adding a Team.
type NewTeam struct {
	ProjectID string `json:"project_id"`
	Name      string `json:"name"`
}

// UpdateTeam defines what information may be provided to modify an
// existing Team. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateTeam struct {
	TeamLeadID *string `json:"team_lead_id"`
	Name       *string `json:"name"`
}

// =============================================================================

func toTeam(dbx db.Team) Team {
	x := (*Team)(unsafe.Pointer(&dbx))
	return *x
}

func toTeamSlice(dbXX []db.Team) []Team {
	xx := make([]Team, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toTeam(dbx)
	}
	return xx
}
