package role

type Role string

var (
	Admin    Role = "ROLE_ADMIN"
	HR       Role = "ROLE_HR"
	Employee Role = "ROLE_EMPLOYEE"
	User     Role = "ROLE_USER"
)
