package complaint_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/complaint"
	"gitlab.com/exmatic/org/business/data/dbtest"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"
)

var c *docker.Container

func Test_Complaint(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := complaint.NewCore(log, db)
	now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

	t.Log("Given the need to work with Complaint records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Complaint.", testID)
		{
			ctx := context.Background()

			np := complaint.NewComplaint{
				CompanyID:    "802be894-d1f3-11ec-baf3-526e5eed5702",
				DepartmentID: "802bec54-d1f3-11ec-baf3-526e5eed5702",
				CreatedBy:    "627d0550f40b33cd4a036585",
				Data:         "test data",
				IsAnonymous:  true,
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Complaint : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Complaint.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Complaint by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Complaint by ID.", dbtest.Success, testID)

			if saved.IsAnonymous && saved.CreatedBy != "" {
				t.Fatalf("\t%s\tTest %d:\tShould not be able to retrieve CreatedBy field by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould not be able to retrieve CreatedBy field by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved, cmpopts.IgnoreFields(complaint.Complaint{}, "CreatedBy")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Complaint.\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Complaint.", dbtest.Success, testID)

			Complaints, err := core.Query(ctx, np.CompanyID, filter.Filters{
				PageNumber:   1,
				RowsPerPage:  3,
				Sort:         "-date_created",
				SortSafeList: []string{"date_created", "-date_created"},
			})
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Complaint : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Complaint.", dbtest.Success, testID)

			if saved.IsAnonymous {
				for _, c2 := range Complaints {
					if c2.CreatedBy != "" {
						t.Fatalf("\t%s\tTest %d:\tShould not be able to retrieve CreatedBy field when getting all complaints: %s.", dbtest.Failed, testID, err)
					}
				}
				t.Logf("\t%s\tTest %d:\tShould not be able to retrieve CreatedBy field when getting all complaints.", dbtest.Success, testID)
			}

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Complaint : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Complaint.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Complaint : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Complaint.", dbtest.Success, testID)
		}
	}
}
