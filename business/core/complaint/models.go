package complaint

import (
	"gitlab.com/exmatic/org/business/core/complaint/db"
	"time"
	"unsafe"
)

type Complaint struct {
	ID           string    `json:"complaint_id"`
	CompanyID    string    `json:"company_id"`
	DepartmentID string    `json:"department_id"`
	CreatedBy    string    `json:"created_by,omitempty"`
	Data         string    `json:"data"`
	IsAnonymous  bool      `json:"is_anonymous"`
	DateCreated  time.Time `json:"date_created"`
}

// NewComplaint is what we require from clients when adding a Complaint.
type NewComplaint struct {
	CompanyID    string `json:"company_id"`
	DepartmentID string `json:"department_id"`
	CreatedBy    string `json:"created_by,omitempty"`
	Data         string `json:"data"`
	IsAnonymous  bool   `json:"is_anonymous"`
}

// =============================================================================

func toComplaint(dbx db.Complaint) Complaint {
	x := (*Complaint)(unsafe.Pointer(&dbx))
	return *x
}

func toComplaintSlice(dbXX []db.Complaint) []Complaint {
	xx := make([]Complaint, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toComplaint(dbx)
	}
	return xx
}
