// Package complaint provides an example of a core business API. Right now these
// calls are just wrapping the data/Store layer. But at some point you will
// want auditing or something that isn't specific to the data/Store layer.
package complaint

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/complaint/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
	"time"
)

// Core manages the set of APIs for Complaint access.
type Core struct {
	Store db.Store
}

// NewCore constructs a core for Complaint api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		Store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Complaint to the database. It returns the created Complaint with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewComplaint, now time.Time) (Complaint, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Complaint{}, err
	}

	dbx := db.Complaint{
		ID:           validate.GenerateID(),
		CompanyID:    usr.Info.CompanyID,
		DepartmentID: x.DepartmentID,
		CreatedBy:    x.CreatedBy,
		Data:         x.Data,
		IsAnonymous:  x.IsAnonymous,
		DateCreated:  now,
	}

	if err := c.Store.Create(ctx, dbx); err != nil {
		return Complaint{}, fmt.Errorf("create: %w", err)
	}

	return toComplaint(dbx), nil
}

// Delete removes the Complaint identified by a given ID.
func (c Core) Delete(ctx context.Context, ComplaintID string) error {
	if err := validate.CheckID(ComplaintID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.Store.Delete(ctx, ComplaintID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Complaints from the database.
func (c Core) Query(ctx context.Context, companyID string, filters filter.Filters) ([]Complaint, error) {
	// Initialize a new Validator instance.
	v := validate.New()

	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	if filter.ValidateFilters(v, filters); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	dbXX, err := c.Store.Query(ctx, companyID, filters)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	// hide who created complaint if it was anonymous.
	for i, _ := range dbXX {
		if dbXX[i].IsAnonymous {
			dbXX[i].CreatedBy = ""
		}
	}

	return toComplaintSlice(dbXX), nil
}

// QueryByID finds the Complaint identified by a given ID.
func (c Core) QueryByID(ctx context.Context, ComplaintID string) (Complaint, error) {
	if err := validate.CheckID(ComplaintID); err != nil {
		return Complaint{}, core.ErrInvalidID
	}

	dbx, err := c.Store.QueryByID(ctx, ComplaintID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Complaint{}, core.ErrNotFound
		}
		return Complaint{}, fmt.Errorf("query: %w", err)
	}

	if dbx.IsAnonymous {
		dbx.CreatedBy = ""
	}

	return toComplaint(dbx), nil
}
