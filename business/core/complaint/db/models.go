package db

import "time"

type Complaint struct {
	ID           string    `db:"complaint_id"`
	CompanyID    string    `db:"company_id"`
	DepartmentID string    `db:"department_id"`
	CreatedBy    string    `db:"created_by"`
	Data         string    `db:"data"`
	IsAnonymous  bool      `db:"is_anonymous"`
	DateCreated  time.Time `db:"date_created"`
}
