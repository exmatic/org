// Package db contains request related CRUD functionality.
package db

import (
	"context"
	"fmt"
	"gitlab.com/exmatic/org/business/sys/filter"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	Log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		Log: log,
		db:  db,
	}
}

// Create adds a request to the database. It returns the created request with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Complaint) error {
	const q = `
	INSERT INTO complaints
		(complaint_id, company_id, department_id, created_by, data, is_anonymous, date_created)
	VALUES
		(:complaint_id, :company_id, :department_id, :created_by, :data, :is_anonymous, :date_created)`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting Complaint: %w", err)
	}

	return nil
}

// Delete removes the request identified by a given ID.
func (s Store) Delete(ctx context.Context, ComplaintID string) error {
	data := struct {
		ComplaintID string `db:"complaint_id"`
	}{
		ComplaintID: ComplaintID,
	}

	const q = `
	DELETE FROM
		complaints
	WHERE
		complaint_id = :complaint_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting Complaint ComplaintID[%v]: %w", ComplaintID, err)
	}

	return nil
}

// Query gets all request from the database.
func (s Store) Query(ctx context.Context, companyID string, filters filter.Filters) ([]Complaint, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (filters.PageNumber - 1) * filters.RowsPerPage,
		RowsPerPage: filters.RowsPerPage,
	}

	q := fmt.Sprintf(`
	SELECT 
		*
	FROM 
		complaints
	WHERE
		company_id = :company_id
	ORDER BY
		%s %s
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`,
		filters.SortColumn(), filters.SortDirection())

	var xx []Complaint
	if err := database.NamedQuerySlice(ctx, s.Log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting request: %w", err)
	}

	return xx, nil
}

// QueryByID finds the request identified by a given ID.
func (s Store) QueryByID(ctx context.Context, ComplaintID string) (Complaint, error) {
	data := struct {
		ComplaintID string `db:"complaint_id"`
	}{
		ComplaintID: ComplaintID,
	}

	const q = `
	SELECT 
		*
	FROM 
		complaints
	WHERE 
		complaint_id = :complaint_id`

	var x Complaint
	if err := database.NamedQueryStruct(ctx, s.Log, s.db, q, data, &x); err != nil {
		return Complaint{}, fmt.Errorf("selecting Complaint ComplaintID[%v]: %w", ComplaintID, err)
	}

	return x, nil
}
