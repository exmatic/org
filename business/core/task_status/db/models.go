package db

type TaskStatus struct {
	ID                  string `db:"task_status_id"`
	CompanyID           string `db:"company_id"`
	PercentageCompleted int    `db:"percentage_completed"`
	Name                string `db:"name"`
}
