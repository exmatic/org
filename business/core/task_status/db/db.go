// Package db contains task_status related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a task_status to the database. It returns the created task_status with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x TaskStatus) error {
	const q = `
	INSERT INTO task_status
		(task_status_id, company_id, name, percentage_completed)
	VALUES
		(:task_status_id, :company_id, :name, :percentage_completed)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting task_status: %w", err)
	}

	return nil
}

// Update modifies data about a task_status. It will error if the specified ID is
// invalid or does not reference an existing task_status.
func (s Store) Update(ctx context.Context, x TaskStatus) error {
	const q = `
	UPDATE
		task_status
	SET
		"name" = :name,
		"percentage_completed" = :percentage_completed
	WHERE
		task_status_id = :task_status_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating task_status task_statusID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the task_status identified by a given ID.
func (s Store) Delete(ctx context.Context, TaskStatusID string) error {
	data := struct {
		TaskStatusID string `db:"task_status_id"`
	}{
		TaskStatusID: TaskStatusID,
	}

	const q = `
	DELETE FROM
		task_status
	WHERE
		task_status_id = :task_status_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting task_status request_typeID[%v]: %w", TaskStatusID, err)
	}

	return nil
}

// Query gets all task_status from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]TaskStatus, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM task_status
	WHERE
		company_id = :company_id
	ORDER BY
		percentage_completed ASC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []TaskStatus
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting task_status: %w", err)
	}

	return xx, nil
}

// QueryByID finds the task_status identified by a given ID.
func (s Store) QueryByID(ctx context.Context, TaskStatusID string) (TaskStatus, error) {
	data := struct {
		TaskStatusID string `db:"task_status_id"`
	}{
		TaskStatusID: TaskStatusID,
	}

	const q = `
	SELECT * FROM task_status
	WHERE
		task_status_id = :task_status_id`

	var x TaskStatus
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return TaskStatus{}, fmt.Errorf("selecting task_status task_statusID[%v]: %w", TaskStatusID, err)
	}

	return x, nil
}
