// Package task_status provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package task_status

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/task_status/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for TaskStatus access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for TaskStatus api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a TaskStatus to the database. It returns the created TaskStatus with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewTaskStatus) (TaskStatus, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return TaskStatus{}, err
	}

	dbx := db.TaskStatus{
		ID:                  validate.GenerateID(),
		CompanyID:           usr.Info.CompanyID,
		PercentageCompleted: x.PercentageCompleted,
		Name:                x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return TaskStatus{}, fmt.Errorf("create: %w", err)
	}

	return toTaskStatus(dbx), nil
}

// Update modifies data about a TaskStatus. It will error if the specified ID is
// invalid or does not reference an existing TaskStatus.
func (c Core) Update(ctx context.Context, TaskStatusID string, up UpdateTaskStatus) error {
	if err := validate.CheckID(TaskStatusID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskStatusID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating TaskStatus TaskStatusID[%v]: %w", TaskStatusID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}
	if up.PercentageCompleted != nil {
		dbx.PercentageCompleted = *up.PercentageCompleted
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the TaskStatus identified by a given ID.
func (c Core) Delete(ctx context.Context, TaskStatusID string) error {
	if err := validate.CheckID(TaskStatusID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, TaskStatusID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all TaskStatuss from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]TaskStatus, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toTaskStatusSlice(dbXX), nil
}

// QueryByID finds the TaskStatus identified by a given ID.
func (c Core) QueryByID(ctx context.Context, TaskStatusID string) (TaskStatus, error) {
	if err := validate.CheckID(TaskStatusID); err != nil {
		return TaskStatus{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskStatusID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return TaskStatus{}, core.ErrNotFound
		}
		return TaskStatus{}, fmt.Errorf("query: %w", err)
	}

	return toTaskStatus(dbx), nil
}
