package task_status

import (
	"gitlab.com/exmatic/org/business/core/task_status/db"
	"unsafe"
)

type TaskStatus struct {
	ID                  string `json:"request_type_id"`
	CompanyID           string `json:"company_id"`
	PercentageCompleted int    `json:"percentage_completed"`
	Name                string `json:"name"`
}

// NewTaskStatus is what we require from clients when adding a TaskStatus.
type NewTaskStatus struct {
	CompanyID           string `json:"company_id"`
	PercentageCompleted int    `json:"percentage_completed"`
	Name                string `json:"name"`
}

// UpdateTaskStatus defines what information may be provided to modify an
// existing TaskStatus. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateTaskStatus struct {
	PercentageCompleted *int    `json:"percentage_completed"`
	Name                *string `json:"name"`
}

// =============================================================================

func toTaskStatus(dbx db.TaskStatus) TaskStatus {
	x := (*TaskStatus)(unsafe.Pointer(&dbx))
	return *x
}

func toTaskStatusSlice(dbXX []db.TaskStatus) []TaskStatus {
	xx := make([]TaskStatus, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toTaskStatus(dbx)
	}
	return xx
}
