// Package candidate provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package candidate

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/filter"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/candidate/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Candidate access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Candidate api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Candidate to the database. It returns the created Candidate with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewCandidate, now time.Time) (Candidate, error) {
	if x.CV == "" {
		return Candidate{}, validate.FieldErrors{
			validate.FieldError{
				Field: "cv",
				Error: core.ErrFieldRequired.Error(),
			},
		}
	}
	if x.Skills == nil {
		x.Skills = []string{}
	}

	cv, err := base64.StdEncoding.DecodeString(x.CV)
	if err != nil {
		return Candidate{}, validate.FieldErrors{
			validate.FieldError{
				Field: "cv",
				Error: err.Error(),
			},
		}
	}

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Candidate{}, err
	}

	dbx := db.Candidate{
		ID:          validate.GenerateID(),
		CompanyID:   usr.Info.CompanyID,
		CreatorID:   usr.ID,
		VacancyID:   x.VacancyID,
		CV:          cv,
		Skills:      x.Skills,
		Notes:       x.Notes,
		DateCreated: now,
		DateUpdated: now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Candidate{}, fmt.Errorf("create: %w", err)
	}

	return toCandidate(dbx), nil
}

// Update modifies data about a Candidate. It will error if the specified ID is
// invalid or does not reference an existing Candidate.
func (c Core) Update(ctx context.Context, CandidateID string, up UpdateCandidate) error {
	if err := validate.CheckID(CandidateID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, CandidateID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Candidate CandidateID[%v]: %w", CandidateID, err)
	}

	if up.Skills != nil {
		dbx.Skills = up.Skills
	}
	if up.CV != nil {
		cv, err := base64.StdEncoding.DecodeString(*up.CV)
		if err != nil {
			return validate.FieldErrors{
				validate.FieldError{
					Field: "cv",
					Error: err.Error(),
				},
			}
		}

		dbx.CV = cv
	}
	if up.Notes != nil {
		dbx.Notes = *up.Notes
	}
	if up.VacancyID != nil {
		dbx.VacancyID = *up.VacancyID
	}
	dbx.DateUpdated = time.Now()

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Candidate identified by a given ID.
func (c Core) Delete(ctx context.Context, CandidateID string) error {
	if err := validate.CheckID(CandidateID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, CandidateID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Requests from the database.
func (c Core) Query(ctx context.Context, companyID string, filters filter.Filters, filterCandidate db.FilterCandidate) ([]Candidate, error) {
	// Initialize a new Validator instance.
	v := validate.New()

	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	if db.ValidateFilterCandidate(v, filterCandidate); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	if filter.ValidateFilters(v, filters); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	dbXX, err := c.store.Query(ctx, companyID, filters, filterCandidate)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toCandidateSlice(dbXX), nil
}

// QueryByID finds the Candidate identified by a given ID.
func (c Core) QueryByID(ctx context.Context, CandidateID string) (Candidate, error) {
	if err := validate.CheckID(CandidateID); err != nil {
		return Candidate{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, CandidateID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Candidate{}, core.ErrNotFound
		}
		return Candidate{}, fmt.Errorf("query: %w", err)
	}

	return toCandidate(dbx), nil
}
