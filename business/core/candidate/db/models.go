package db

import (
	"github.com/lib/pq"
	"gitlab.com/exmatic/org/business/sys/validate"
	"time"
)

type Candidate struct {
	ID          string         `db:"candidate_id"`
	CompanyID   string         `db:"company_id"`
	CreatorID   string         `db:"creator_id"`
	VacancyID   string         `db:"vacancy_id"`
	CV          []byte         `db:"cv"`
	Skills      pq.StringArray `db:"skills"`
	Notes       string         `db:"notes"`
	DateCreated time.Time      `db:"date_created"`
	DateUpdated time.Time      `db:"date_updated"`
}

type FilterCandidate struct {
	CreatorID string         `db:"creator_id"`
	VacancyID string         `db:"vacancy_id"`
	Skills    pq.StringArray `db:"skills"`
}

func ValidateFilterCandidate(v *validate.Validator, x FilterCandidate) {
	if x.VacancyID != "" {
		if err := validate.CheckID(x.VacancyID); err != nil {
			v.Check(false, "vacancy_id", err.Error())
		}
	}
}
