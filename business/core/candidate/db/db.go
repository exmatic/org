// Package db contains candidate related CRUD functionality.
package db

import (
	"context"
	"fmt"
	"gitlab.com/exmatic/org/business/sys/filter"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a candidate to the database. It returns the created candidate with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Candidate) error {
	const q = `
	INSERT INTO candidate
		(candidate_id, company_id, creator_id, vacancy_id, cv, skills, notes, date_created, date_updated)
	VALUES
		(:candidate_id, :company_id, :creator_id, :vacancy_id, :cv, :skills, :notes, :date_created, :date_updated)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting candidate: %w", err)
	}

	return nil
}

// Update modifies data about a candidate. It will error if the specified ID is
// invalid or does not reference an existing candidate.
func (s Store) Update(ctx context.Context, x Candidate) error {
	const q = `
	UPDATE
		candidate
	SET
		"vacancy_id" = :vacancy_id,
		"cv" = :cv,
		"skills" = :skills,
		"notes" = :notes
	WHERE
		candidate_id = :candidate_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating candidate candidateID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the candidate identified by a given ID.
func (s Store) Delete(ctx context.Context, candidateID string) error {
	data := struct {
		CandidateID string `db:"candidate_id"`
	}{
		CandidateID: candidateID,
	}

	const q = `
	DELETE FROM
		candidate
	WHERE
		candidate_id = :candidate_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting candidate candidateID[%v]: %w", candidateID, err)
	}

	return nil
}

// Query gets all request from the database.
func (s Store) Query(ctx context.Context, companyID string, filters filter.Filters, filterCandidate FilterCandidate) ([]Candidate, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		FilterCandidate
	}{
		CompanyID:       companyID,
		Offset:          (filters.PageNumber - 1) * filters.RowsPerPage,
		RowsPerPage:     filters.RowsPerPage,
		FilterCandidate: filterCandidate,
	}

	q := fmt.Sprintf(`
	SELECT 
		*
	FROM 
		candidate
	WHERE
		(company_id = :company_id) AND
		(
			(:creator_id = '' OR creator_id = :creator_id) AND
			(:vacancy_id = '' OR CAST(vacancy_id as text) = :vacancy_id) AND
			(:skills = '{}' OR skills @> :skills)
		)
	ORDER BY
		date_created DESC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`)

	var xx []Candidate
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting request: %w", err)
	}

	return xx, nil
}

// QueryByID finds the candidate identified by a given ID.
func (s Store) QueryByID(ctx context.Context, candidateID string) (Candidate, error) {
	data := struct {
		CandidateID string `db:"candidate_id"`
	}{
		CandidateID: candidateID,
	}

	const q = `
	SELECT * FROM candidate
	WHERE
		candidate_id = :candidate_id`

	var x Candidate
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Candidate{}, fmt.Errorf("selecting candidate candidateID[%v]: %w", candidateID, err)
	}

	return x, nil
}
