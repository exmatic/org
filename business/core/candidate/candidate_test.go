package candidate_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/lib/pq"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/candidate"
	candidatedb "gitlab.com/exmatic/org/business/core/candidate/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/exmatic/org/business/data/dbtest"
)

var c *docker.Container

func Test_Candidate(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := candidate.NewCore(log, db)

	t.Log("Given the need to work with Candidate records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Candidate.", testID)
		{
			ctx := context.Background()
			now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

			var b64 = `TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz
IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg
dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu
dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo
ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=`
			//cv, err := base64.StdEncoding.DecodeString(b64)

			np := candidate.NewCandidate{
				CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702",
				CreatorID: "test id",
				VacancyID: "802bec90-d1f3-11ec-baf3-526e5eed5702",
				CV:        b64,
				Skills:    pq.StringArray{"python", "c++"},
				Notes:     "test notes",
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Candidate : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Candidate.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Candidate by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Candidate by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Candidate. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Candidate.", dbtest.Success, testID)

			upd := candidate.UpdateCandidate{
				Skills: pq.StringArray{"python", "c++", "java"},
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update Candidate : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update Candidate.", dbtest.Success, testID)

			Candidates, err := core.Query(ctx, x.CompanyID, filter.Filters{
				PageNumber:   1,
				RowsPerPage:  3,
				Sort:         "-date_created",
				SortSafeList: []string{"date_created", "-date_created"},
			}, candidatedb.FilterCandidate{
				CreatorID: "",
				VacancyID: "",
				Skills:    pq.StringArray{"python"},
			})
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Candidate : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Candidate.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original Candidate
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Skills = upd.Skills

			var idx int
			for i, p := range Candidates {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, Candidates[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Candidate. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Candidate.", dbtest.Success, testID)

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Candidate : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Candidate.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Candidate : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Candidate.", dbtest.Success, testID)
		}
	}
}
