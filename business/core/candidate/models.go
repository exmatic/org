package candidate

import (
	"encoding/base64"
	"gitlab.com/exmatic/org/business/core/candidate/db"
	"time"
)

type Candidate struct {
	ID          string    `json:"candidate_id"`
	CompanyID   string    `json:"company_id"`
	CreatorID   string    `json:"creator_id"`
	VacancyID   string    `json:"vacancy_id"`
	CV          string    `json:"cv"` // should be base64 represented as string
	Skills      []string  `json:"skills"`
	Notes       string    `json:"notes"`
	DateCreated time.Time `json:"date_created"`
	DateUpdated time.Time `json:"date_updated"`
}

// NewCandidate is what we require from clients when adding a Candidate.
type NewCandidate struct {
	CompanyID string   `json:"company_id"`
	CreatorID string   `json:"creator_id"`
	VacancyID string   `json:"vacancy_id"`
	CV        string   `json:"cv"` // should be base64 represented as string
	Skills    []string `json:"skills"`
	Notes     string   `json:"notes,omitempty"`
}

// UpdateCandidate defines what information may be provided to modify an
// existing Candidate. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateCandidate struct {
	VacancyID *string  `json:"vacancy_id"`
	CV        *string  `json:"cv"`
	Skills    []string `json:"skills"`
	Notes     *string  `json:"notes,omitempty"`
}

// =============================================================================

func toCandidate(dbx db.Candidate) Candidate {
	x := Candidate{
		ID:          dbx.ID,
		CompanyID:   dbx.CompanyID,
		CreatorID:   dbx.CompanyID,
		VacancyID:   dbx.VacancyID,
		CV:          base64.StdEncoding.EncodeToString(dbx.CV),
		Skills:      dbx.Skills,
		Notes:       dbx.Notes,
		DateCreated: dbx.DateCreated,
		DateUpdated: dbx.DateUpdated,
	}

	return x
}

func toCandidateSlice(dbXX []db.Candidate) []Candidate {
	xx := make([]Candidate, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toCandidate(dbx)
	}
	return xx
}
