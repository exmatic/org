package request_type

import (
	"gitlab.com/exmatic/org/business/core/request_type/db"
	"unsafe"
)

type RequestType struct {
	ID        string `json:"request_type_id"`
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// NewRequestType is what we require from clients when adding a RequestType.
type NewRequestType struct {
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// UpdateRequestType defines what information may be provided to modify an
// existing RequestType. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateRequestType struct {
	Name *string `json:"name"`
}

// =============================================================================

func toRequestType(dbx db.RequestType) RequestType {
	x := (*RequestType)(unsafe.Pointer(&dbx))
	return *x
}

func toRequestTypeSlice(dbXX []db.RequestType) []RequestType {
	xx := make([]RequestType, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toRequestType(dbx)
	}
	return xx
}
