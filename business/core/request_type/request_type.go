// Package request_type provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package request_type

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/request_type/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for RequestType access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for RequestType api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a RequestType to the database. It returns the created RequestType with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewRequestType) (RequestType, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return RequestType{}, err
	}

	dbx := db.RequestType{
		ID:        validate.GenerateID(),
		CompanyID: usr.Info.CompanyID,
		Name:      x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return RequestType{}, fmt.Errorf("create: %w", err)
	}

	return toRequestType(dbx), nil
}

// Update modifies data about a RequestType. It will error if the specified ID is
// invalid or does not reference an existing RequestType.
func (c Core) Update(ctx context.Context, RequestTypeID string, up UpdateRequestType) error {
	if err := validate.CheckID(RequestTypeID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, RequestTypeID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating RequestType RequestTypeID[%v]: %w", RequestTypeID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the RequestType identified by a given ID.
func (c Core) Delete(ctx context.Context, RequestTypeID string) error {
	if err := validate.CheckID(RequestTypeID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, RequestTypeID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all RequestTypes from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]RequestType, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toRequestTypeSlice(dbXX), nil
}

// QueryByID finds the RequestType identified by a given ID.
func (c Core) QueryByID(ctx context.Context, RequestTypeID string) (RequestType, error) {
	if err := validate.CheckID(RequestTypeID); err != nil {
		return RequestType{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, RequestTypeID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return RequestType{}, core.ErrNotFound
		}
		return RequestType{}, fmt.Errorf("query: %w", err)
	}

	return toRequestType(dbx), nil
}
