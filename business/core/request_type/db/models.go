package db

type RequestType struct {
	ID        string `db:"request_type_id"`
	CompanyID string `db:"company_id"`
	Name      string `db:"name"`
}
