// Package db contains request_types related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a request_types to the database. It returns the created request_types with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x RequestType) error {
	const q = `
	INSERT INTO request_types
		(request_type_id, company_id, name)
	VALUES
		(:request_type_id, :company_id, :name)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting request_types: %w", err)
	}

	return nil
}

// Update modifies data about a request_types. It will error if the specified ID is
// invalid or does not reference an existing request_types.
func (s Store) Update(ctx context.Context, x RequestType) error {
	const q = `
	UPDATE
		request_types
	SET
		"name" = :name
	WHERE
		request_type_id = :request_type_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating request_types request_typesID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the request_types identified by a given ID.
func (s Store) Delete(ctx context.Context, RequestTypeID string) error {
	data := struct {
		RequestTypeID string `db:"request_type_id"`
	}{
		RequestTypeID: RequestTypeID,
	}

	const q = `
	DELETE FROM
		request_types
	WHERE
		request_type_id = :request_type_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting request_types request_typeID[%v]: %w", RequestTypeID, err)
	}

	return nil
}

// Query gets all request_types from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]RequestType, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM request_types
	WHERE
		company_id = :company_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []RequestType
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting request_types: %w", err)
	}

	return xx, nil
}

// QueryByID finds the request_types identified by a given ID.
func (s Store) QueryByID(ctx context.Context, requestTypeID string) (RequestType, error) {
	data := struct {
		RequestTypeID string `db:"request_type_id"`
	}{
		RequestTypeID: requestTypeID,
	}

	const q = `
	SELECT * FROM request_types
	WHERE
		request_type_id = :request_type_id`

	var x RequestType
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return RequestType{}, fmt.Errorf("selecting request_types request_typesID[%v]: %w", requestTypeID, err)
	}

	return x, nil
}
