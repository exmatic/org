// Package position provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package position

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/position/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Position access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Position api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Position to the database. It returns the created Position with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewPosition) (Position, error) {

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Position{}, err
	}

	dbx := db.Position{
		ID:        validate.GenerateID(),
		CompanyID: usr.Info.CompanyID,
		Name:      x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Position{}, fmt.Errorf("create: %w", err)
	}

	return toPosition(dbx), nil
}

// Update modifies data about a Position. It will error if the specified ID is
// invalid or does not reference an existing Position.
func (c Core) Update(ctx context.Context, PositionID string, up UpdatePosition) error {
	if err := validate.CheckID(PositionID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, PositionID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Position PositionID[%v]: %w", PositionID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Position identified by a given ID.
func (c Core) Delete(ctx context.Context, PositionID string) error {
	if err := validate.CheckID(PositionID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, PositionID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Position from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Position, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toPositionSlice(dbXX), nil
}

// QueryByID finds the Position identified by a given ID.
func (c Core) QueryByID(ctx context.Context, PositionID string) (Position, error) {
	if err := validate.CheckID(PositionID); err != nil {
		return Position{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, PositionID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Position{}, core.ErrNotFound
		}
		return Position{}, fmt.Errorf("query: %w", err)
	}

	return toPosition(dbx), nil
}
