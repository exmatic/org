package db

type Position struct {
	ID        string `db:"position_id"`
	CompanyID string `db:"company_id"`
	Name      string `db:"name"`
}
