package position

import (
	"gitlab.com/exmatic/org/business/core/position/db"
	"unsafe"
)

type Position struct {
	ID        string `db:"position_id"`
	CompanyID string `db:"company_id"`
	Name      string `db:"name"`
}

// NewPosition is what we require from clients when adding a Position.
type NewPosition struct {
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// UpdatePosition defines what information may be provided to modify an
// existing Position. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdatePosition struct {
	Name *string `json:"name"`
}

// =============================================================================

func toPosition(dbx db.Position) Position {
	x := (*Position)(unsafe.Pointer(&dbx))
	return *x
}

func toPositionSlice(dbXX []db.Position) []Position {
	xx := make([]Position, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toPosition(dbx)
	}
	return xx
}
