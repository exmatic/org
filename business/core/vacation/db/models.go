package db

import (
	"gitlab.com/exmatic/org/business/sys/validate"
	"time"
)

type Vacation struct {
	ID            string    `db:"vacation_id"`
	CompanyID     string    `db:"company_id"`
	DepartmentID  string    `db:"department_id"`
	RequestTypeID string    `db:"request_type_id"`
	UserID        string    `db:"user_id"`
	DateSince     time.Time `db:"date_since"`
	DateTo        time.Time `db:"date_to"`
}

type JoinVacation struct {
	Vacation
	RequestTypeName string `db:"name"`
}

type FilterVacation struct {
	DepartmentID  string `db:"department_id"`
	RequestTypeID string `db:"request_type_id"`
	UserID        string `db:"user_id"`
}

func ValidateFilterVacation(v *validate.Validator, x FilterVacation) {
	if x.DepartmentID != "" {
		if err := validate.CheckID(x.DepartmentID); err != nil {
			v.Check(false, "department_id", err.Error())
		}
	}
	if x.RequestTypeID != "" {
		if err := validate.CheckID(x.RequestTypeID); err != nil {
			v.Check(false, "request_type_id", err.Error())
		}
	}
}
