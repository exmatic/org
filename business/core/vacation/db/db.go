// Package db contains request related CRUD functionality.
package db

import (
	"context"
	"fmt"
	"gitlab.com/exmatic/org/business/sys/filter"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	Log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		Log: log,
		db:  db,
	}
}

// Create adds a request to the database. It returns the created request with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Vacation) error {
	const q = `
	INSERT INTO vacations
		(vacation_id, company_id, department_id, request_type_id, user_id, date_since, date_to)
	VALUES
		(:vacation_id, :company_id, :department_id, :request_type_id, :user_id, :date_since, :date_to)`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting vacation: %w", err)
	}

	return nil
}

// Delete removes the request identified by a given ID.
func (s Store) Delete(ctx context.Context, vacationID string) error {
	data := struct {
		VacationID string `db:"vacation_id"`
	}{
		VacationID: vacationID,
	}

	const q = `
	DELETE FROM
		vacations
	WHERE
		vacation_id = :vacation_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting vacation vacationID[%v]: %w", vacationID, err)
	}

	return nil
}

// Query gets all request from the database.
func (s Store) Query(ctx context.Context, companyID string, filters filter.Filters, filterVacation FilterVacation) ([]JoinVacation, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		FilterVacation
	}{
		CompanyID:      companyID,
		Offset:         (filters.PageNumber - 1) * filters.RowsPerPage,
		RowsPerPage:    filters.RowsPerPage,
		FilterVacation: filterVacation,
	}

	q := fmt.Sprintf(`
	SELECT 
		v.*,
		rt.name
	FROM 
		vacations as v
	JOIN 
		request_types as rt on v.request_type_id = rt.request_type_id
	WHERE
		(v.company_id = :company_id) AND
		(
			(:department_id = '' OR CAST(v.department_id as text) = :department_id) AND
			(:request_type_id = '' OR CAST(v.request_type_id as text) = :request_type_id) AND
			(:user_id = '' OR v.user_id = :user_id)
		)
	ORDER BY
		v.%s %s, v.vacation_id ASC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`,
		filters.SortColumn(), filters.SortDirection())

	var xx []JoinVacation
	if err := database.NamedQuerySlice(ctx, s.Log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting request: %w", err)
	}

	return xx, nil
}

// QueryByID finds the request identified by a given ID.
func (s Store) QueryByID(ctx context.Context, vacationID string) (JoinVacation, error) {
	data := struct {
		VacationID string `db:"vacation_id"`
	}{
		VacationID: vacationID,
	}

	const q = `
	SELECT 
		v.*,
		rt.name
	FROM 
		vacations as v
	JOIN 
		request_types as rt on v.request_type_id = rt.request_type_id
	WHERE 
		v.vacation_id = :vacation_id`

	var x JoinVacation
	if err := database.NamedQueryStruct(ctx, s.Log, s.db, q, data, &x); err != nil {
		return JoinVacation{}, fmt.Errorf("selecting vacation vacationID[%v]: %w", vacationID, err)
	}

	return x, nil
}
