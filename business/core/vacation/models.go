package vacation

import (
	"gitlab.com/exmatic/org/business/core/vacation/db"
	"time"
	"unsafe"
)

type Vacation struct {
	ID            string    `json:"vacation_id"`
	CompanyID     string    `json:"company_id"`
	DepartmentID  string    `json:"department_id"`
	RequestTypeID string    `json:"request_type_id"`
	UserID        string    `json:"user_id"`
	DateSince     time.Time `json:"date_since"`
	DateTo        time.Time `json:"date_to"`
}

type JoinVacation struct {
	Vacation
	RequestTypeName string `json:"request_type_name"`
}

// NewVacation is what we require from clients when adding a Vacation.
type NewVacation struct {
	CompanyID     string    `json:"company_id"`
	DepartmentID  string    `json:"department_id"`
	RequestTypeID string    `json:"request_type_id"`
	UserID        string    `json:"user_id"`
	DateSince     time.Time `json:"date_since"`
	DateTo        time.Time `json:"date_to"`
}

// =============================================================================

func toVacation(dbx db.Vacation) Vacation {
	x := (*Vacation)(unsafe.Pointer(&dbx))
	return *x
}

func toJoinVacation(dbx db.JoinVacation) JoinVacation {
	x := (*JoinVacation)(unsafe.Pointer(&dbx))
	return *x
}

func toVacationSlice(dbXX []db.Vacation) []Vacation {
	xx := make([]Vacation, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toVacation(dbx)
	}
	return xx
}

func toJoinVacationSlice(dbXX []db.JoinVacation) []JoinVacation {
	xx := make([]JoinVacation, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toJoinVacation(dbx)
	}
	return xx
}
