// Package vacation provides an example of a core business API. Right now these
// calls are just wrapping the data/Store layer. But at some point you will
// want auditing or something that isn't specific to the data/Store layer.
package vacation

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/core/vacation/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Vacation access.
type Core struct {
	Store db.Store
}

// NewCore constructs a core for Vacation api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		Store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Vacation to the database. It returns the created Vacation with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewVacation) (Vacation, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Vacation{}, err
	}

	dbx := db.Vacation{
		ID:            validate.GenerateID(),
		CompanyID:     usr.Info.CompanyID,
		DepartmentID:  x.DepartmentID,
		RequestTypeID: x.RequestTypeID,
		UserID:        x.UserID,
		DateSince:     x.DateSince,
		DateTo:        x.DateTo,
	}

	if err := c.Store.Create(ctx, dbx); err != nil {
		return Vacation{}, fmt.Errorf("create: %w", err)
	}

	return toVacation(dbx), nil
}

// Delete removes the Vacation identified by a given ID.
func (c Core) Delete(ctx context.Context, VacationID string) error {
	if err := validate.CheckID(VacationID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.Store.Delete(ctx, VacationID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Vacations from the database.
func (c Core) Query(ctx context.Context, companyID string, filters filter.Filters, filterVacation db.FilterVacation) ([]JoinVacation, error) {
	// Initialize a new Validator instance.
	v := validate.New()

	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	if db.ValidateFilterVacation(v, filterVacation); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	if filter.ValidateFilters(v, filters); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	dbXX, err := c.Store.Query(ctx, companyID, filters, filterVacation)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toJoinVacationSlice(dbXX), nil
}

// QueryByID finds the Vacation identified by a given ID.
func (c Core) QueryByID(ctx context.Context, VacationID string) (JoinVacation, error) {
	if err := validate.CheckID(VacationID); err != nil {
		return JoinVacation{}, core.ErrInvalidID
	}

	dbx, err := c.Store.QueryByID(ctx, VacationID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return JoinVacation{}, core.ErrNotFound
		}
		return JoinVacation{}, fmt.Errorf("query: %w", err)
	}

	return toJoinVacation(dbx), nil
}
