package vacation_test

import (
	"context"
	"errors"
	"fmt"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/vacation"
	vacdb "gitlab.com/exmatic/org/business/core/vacation/db"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/exmatic/org/business/data/dbtest"
)

var c *docker.Container

func Test_Vacation(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := vacation.NewCore(log, db)

	t.Log("Given the need to work with Vacation records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Vacation.", testID)
		{
			ctx := context.Background()

			np := vacation.NewVacation{
				CompanyID:     "802be894-d1f3-11ec-baf3-526e5eed5702",
				DepartmentID:  "802bec54-d1f3-11ec-baf3-526e5eed5702",
				RequestTypeID: "802bec86-d1f3-11ec-baf3-526e5eed5702",
				UserID:        "802bec59-d1f3-11ec-baf3-526e5eed5713",
				DateSince:     time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
				DateTo:        time.Date(2019, time.January, 2, 0, 0, 0, 0, time.UTC),
			}

			x, err := core.Create(ctx, np)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Vacation : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Vacation.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Vacation by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Vacation by ID.", dbtest.Success, testID)

			xToJoinVac := vacation.JoinVacation{
				Vacation:        x,
				RequestTypeName: saved.RequestTypeName,
			}

			if diff := cmp.Diff(xToJoinVac, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Vacation.\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Vacation.", dbtest.Success, testID)

			Vacations, err := core.Query(ctx, np.CompanyID, filter.Filters{
				PageNumber:   1,
				RowsPerPage:  3,
				Sort:         "date_since",
				SortSafeList: []string{"date_since", "-date_since"},
			}, vacdb.FilterVacation{})
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Vacation : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Vacation.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original Vacation
			// and change just the fields we expect then diff it with what was saved.
			want := xToJoinVac

			var idx int
			for i, p := range Vacations {
				if p.ID == want.ID {
					idx = i
				}
			}

			if diff := cmp.Diff(want, Vacations[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Vacation. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Vacation.", dbtest.Success, testID)

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Vacation : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Vacation.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Vacation : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Vacation.", dbtest.Success, testID)
		}
	}
}
