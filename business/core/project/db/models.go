package db

import "time"

type Project struct {
	ID               string    `db:"project_id"`
	DepartmentID     string    `db:"department_id"`
	ProjectManagerID string    `db:"project_manager_id"`
	Name             string    `db:"name"`
	DateCreated      time.Time `db:"date_created"`
}
