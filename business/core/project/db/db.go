// Package db contains project related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a project to the database. It returns the created project with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Project) error {
	const q = `
	INSERT INTO project
		(project_id, department_id, project_manager_id, name, date_created)
	VALUES
		(:project_id, :department_id, :project_manager_id, :name, :date_created)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting project: %w", err)
	}

	return nil
}

// Update modifies data about a project. It will error if the specified ID is
// invalid or does not reference an existing project.
func (s Store) Update(ctx context.Context, x Project) error {
	const q = `
	UPDATE
		project
	SET
		"project_manager_id" = :project_manager_id,
		"name" = :name
	WHERE
		project_id = :project_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating project projectID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the project identified by a given ID.
func (s Store) Delete(ctx context.Context, projectID string) error {
	data := struct {
		ProjectID string `db:"project_id"`
	}{
		ProjectID: projectID,
	}

	const q = `
	DELETE FROM
		project
	WHERE
		project_id = :project_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting project projectID[%v]: %w", projectID, err)
	}

	return nil
}

// Query gets all project from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, departmentID string) ([]Project, error) {
	data := struct {
		DepartmentID string `db:"department_id"`
		Offset       int    `db:"offset"`
		RowsPerPage  int    `db:"rows_per_page"`
	}{
		DepartmentID: departmentID,
		Offset:       (pageNumber - 1) * rowsPerPage,
		RowsPerPage:  rowsPerPage,
	}

	const q = `
	SELECT * FROM project
	WHERE
		department_id = :department_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Project
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting project: %w", err)
	}

	return xx, nil
}

// QueryByID finds the project identified by a given ID.
func (s Store) QueryByID(ctx context.Context, projectID string) (Project, error) {
	data := struct {
		ProjectID string `db:"project_id"`
	}{
		ProjectID: projectID,
	}

	const q = `
	SELECT * FROM project
	WHERE
		project_id = :project_id`

	var x Project
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Project{}, fmt.Errorf("selecting project projectID[%v]: %w", projectID, err)
	}

	return x, nil
}
