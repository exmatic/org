// Package project provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package project

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/project/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Project access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Project api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Project to the database. It returns the created Project with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewProject, now time.Time) (Project, error) {
	dbx := db.Project{
		ID:           validate.GenerateID(),
		DepartmentID: x.DepartmentID,
		Name:         x.Name,
		DateCreated:  now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Project{}, fmt.Errorf("create: %w", err)
	}

	return toProject(dbx), nil
}

// Update modifies data about a Project. It will error if the specified ID is
// invalid or does not reference an existing Project.
func (c Core) Update(ctx context.Context, ProjectID string, up UpdateProject) error {
	if err := validate.CheckID(ProjectID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, ProjectID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Project ProjectID[%v]: %w", ProjectID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}
	if up.ProjectManagerID != nil {
		dbx.ProjectManagerID = *up.ProjectManagerID
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Project identified by a given ID.
func (c Core) Delete(ctx context.Context, ProjectID string) error {
	if err := validate.CheckID(ProjectID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, ProjectID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Projects from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, departmentID string) ([]Project, error) {
	if err := validate.CheckID(departmentID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, departmentID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toProjectSlice(dbXX), nil
}

// QueryByID finds the Project identified by a given ID.
func (c Core) QueryByID(ctx context.Context, ProjectID string) (Project, error) {
	if err := validate.CheckID(ProjectID); err != nil {
		return Project{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, ProjectID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Project{}, core.ErrNotFound
		}
		return Project{}, fmt.Errorf("query: %w", err)
	}

	return toProject(dbx), nil
}
