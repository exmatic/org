package project

import (
	"gitlab.com/exmatic/org/business/core/project/db"
	"time"
	"unsafe"
)

type Project struct {
	ID               string    `json:"project_id"`
	DepartmentID     string    `json:"department_id"`
	ProjectManagerID string    `json:"project_manager_id,omitempty"`
	Name             string    `json:"name"`
	DateCreated      time.Time `json:"date_created"`
}

// NewProject is what we require from clients when adding a Project.
type NewProject struct {
	DepartmentID string `json:"department_id"`
	Name         string `json:"name"`
}

// UpdateProject defines what information may be provided to modify an
// existing Project. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateProject struct {
	ProjectManagerID *string `json:"project_manager_id"`
	Name             *string `json:"name"`
}

// =============================================================================

func toProject(dbx db.Project) Project {
	x := (*Project)(unsafe.Pointer(&dbx))
	return *x
}

func toProjectSlice(dbXX []db.Project) []Project {
	xx := make([]Project, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toProject(dbx)
	}
	return xx
}
