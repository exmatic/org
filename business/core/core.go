package core

import (
	"errors"
	"fmt"
	"go.uber.org/zap"
)

const (
	RequestStatusInProgress = "in progress"
	RequestStatusApproved   = "approved"
	RequestStatusDeclined   = "declined"
)

// Set of error variables for CRUD operations.
var (
	ErrInvalidID                = errors.New("ID is not in its proper form")
	ErrNotFound                 = errors.New("Entitiy not found")
	ErrUnsupportedRequestStatus = errors.New("unsupported request status")
	ErrEmailNotVaild            = errors.New("invalid email")
	ErrFieldRequired            = errors.New("field required")
)

// Background is a helper that accepts an arbitrary function as a parameter and runs it in a
// in goroutine in the background.
func Background(logger *zap.SugaredLogger, fn func()) {
	go func() {

		// Recover from any panic
		defer func() {
			if err := recover(); err != nil {
				logger.Error(fmt.Errorf("%s", err), nil)
			}
		}()

		// Execute the arbitrary function that we passed as the parameter
		fn()
	}()
}

func IsSupportedRequestStatus(status string) bool {
	return status == RequestStatusInProgress ||
		status == RequestStatusDeclined ||
		status == RequestStatusApproved
}
