// Package invite provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package invite

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/invite/db"
	"gitlab.com/exmatic/org/business/core/role"
	"gitlab.com/exmatic/org/business/core/user"
	userdb "gitlab.com/exmatic/org/business/core/user/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
	"time"
)

// Core manages the set of APIs for Invite access.
type Core struct {
	store     db.Store
	authStore *userdb.AuthStore
}

// NewCore constructs a core for Invite api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB, authStore *userdb.AuthStore) Core {
	return Core{
		store:     db.NewStore(log, sqlxDB),
		authStore: authStore,
	}
}

// Create adds a Invite to the database. It returns the created Invite with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewInvite, now time.Time) (Invite, error) {

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Invite{}, err
	}

	dbx := db.Invite{
		ID:          validate.GenerateID(),
		CompanyID:   usr.Info.CompanyID,
		UserEmail:   x.UserEmail,
		FromEmail:   usr.Profile.Email,
		Description: x.Description,
		DateCreated: now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Invite{}, fmt.Errorf("create: %w", err)
	}

	return toInvite(dbx), nil
}

func (c Core) UpdateInviteStatus(ctx context.Context, InviteID string, up UpdateInviteStatus) error {
	if err := validate.CheckID(InviteID); err != nil {
		return core.ErrInvalidID
	}

	if ok := core.IsSupportedRequestStatus(up.Status); !ok {
		return core.ErrUnsupportedRequestStatus
	}

	dbx, err := c.store.QueryByID(ctx, InviteID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Invite InviteID[%v]: %w", InviteID, err)
	}

	if err := c.store.UpdateStatus(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	if up.Status != core.RequestStatusApproved {
		return nil
	}

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return err
	}

	vacDays := 0
	now := time.Now()

	ux := user.UpdateUserInfo{
		CompanyID:          &dbx.CompanyID,
		ActiveVacationDays: &vacDays,
		WorkingSince:       &now,
		Role:               &role.Employee,
	}

	err = c.authStore.UpdateUserInfoInit(ctx, &ux, usr.ID)
	if err != nil {
		return err
	}

	return nil
}

// Delete removes the Invite identified by a given ID.
func (c Core) Delete(ctx context.Context, InviteID string) error {
	if err := validate.CheckID(InviteID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, InviteID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

func (c Core) QueryByCompanyID(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Invite, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.QueryByCompanyID(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toInviteSlice(dbXX), nil
}

func (c Core) QueryByEmail(ctx context.Context, pageNumber int, rowsPerPage int, email string) ([]Invite, error) {
	if ok := validate.Matches(email, validate.EmailRX); !ok {
		return nil, core.ErrEmailNotVaild
	}

	dbXX, err := c.store.QueryByUserEmail(ctx, pageNumber, rowsPerPage, email)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toInviteSlice(dbXX), nil
}

// QueryByID finds the Invite identified by a given ID.
func (c Core) QueryByID(ctx context.Context, InviteID string) (Invite, error) {
	if err := validate.CheckID(InviteID); err != nil {
		return Invite{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, InviteID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Invite{}, core.ErrNotFound
		}
		return Invite{}, fmt.Errorf("query: %w", err)
	}

	return toInvite(dbx), nil
}
