package invite

import (
	"gitlab.com/exmatic/org/business/core/invite/db"
	"time"
	"unsafe"
)

type Invite struct {
	ID          string    `json:"invite_id"`
	CompanyID   string    `json:"company_id"`
	UserEmail   string    `json:"user_email"`
	FromEmail   string    `json:"from_email"`
	Description string    `json:"description"`
	Status      string    `json:"status"`
	DateCreated time.Time `json:"date_created"`
}

// NewInvite is what we require from clients when adding a Invite.
type NewInvite struct {
	CompanyID   string `json:"company_id"`
	UserEmail   string `json:"user_email"`
	FromEmail   string `json:"from_email"`
	Description string `json:"description"`
}

// UpdateInviteStatus defines what information may be provided to modify an
// existing Invite. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateInviteStatus struct {
	Status string `json:"status"`
}

// =============================================================================

func toInvite(dbx db.Invite) Invite {
	x := (*Invite)(unsafe.Pointer(&dbx))
	return *x
}

func toInviteSlice(dbXX []db.Invite) []Invite {
	xx := make([]Invite, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toInvite(dbx)
	}
	return xx
}
