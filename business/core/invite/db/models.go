package db

import "time"

type Invite struct {
	ID          string    `db:"invite_id"`
	CompanyID   string    `db:"company_id"`
	UserEmail   string    `db:"user_email"`
	FromEmail   string    `db:"from_email"`
	Description string    `db:"description"`
	Status      string    `db:"status"`
	DateCreated time.Time `db:"date_created"`
}
