// Package db contains Invite related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a Invite to the database. It returns the created Invite with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Invite) error {
	const q = `
	INSERT INTO invites
		(invite_id, company_id, user_email, from_email, description, date_created)
	VALUES
		(:invite_id, :company_id, :user_email, :from_email, :description, :date_created)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting Invite: %w", err)
	}

	return nil
}

// UpdateStatus modifies data about a Invite. It will error if the specified ID is
// invalid or does not reference an existing Invite.
func (s Store) UpdateStatus(ctx context.Context, x Invite) error {
	const q = `
	UPDATE
		invites
	SET
		"status" = :status
	WHERE
		invite_id = :invite_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating Invite InviteID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the Invite identified by a given ID.
func (s Store) Delete(ctx context.Context, InviteID string) error {
	data := struct {
		InviteID string `db:"invite_id"`
	}{
		InviteID: InviteID,
	}

	const q = `
	DELETE FROM
		invites
	WHERE
		invite_id = :invite_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting Invite InviteID[%v]: %w", InviteID, err)
	}

	return nil
}

// QueryByCompanyID gets all Invite from the database.
func (s Store) QueryByCompanyID(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Invite, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM invites
	WHERE
		company_id = :company_id
	ORDER BY
		date_crated DESC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Invite
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting Invite: %w", err)
	}

	return xx, nil
}

// QueryByUserEmail gets all Invite from the database.
func (s Store) QueryByUserEmail(ctx context.Context, pageNumber int, rowsPerPage int, email string) ([]Invite, error) {
	data := struct {
		Email       string `db:"user_email"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		Email:       email,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM invites
	WHERE
		user_email = :user_email
	ORDER BY
		date_created DESC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Invite
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting Invite: %w", err)
	}

	return xx, nil
}

// QueryByID finds the Invite identified by a given ID.
func (s Store) QueryByID(ctx context.Context, InviteID string) (Invite, error) {
	data := struct {
		InviteID string `db:"invite_id"`
	}{
		InviteID: InviteID,
	}

	const q = `
	SELECT * FROM invites
	WHERE
		invite_id = :invite_id`

	var x Invite
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Invite{}, fmt.Errorf("selecting Invite InviteID[%v]: %w", InviteID, err)
	}

	return x, nil
}
