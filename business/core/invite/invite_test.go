package invite_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/invite"
	"gitlab.com/exmatic/org/business/data/dbtest"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"
)

var c *docker.Container

func Test_Invite(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := invite.NewCore(log, db)

	now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

	t.Log("Given the need to work with Invite records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Invite.", testID)
		{
			ctx := context.Background()

			np := invite.NewInvite{
				CompanyID:   "802be894-d1f3-11ec-baf3-526e5eed5702",
				UserEmail:   "testUserEmail@mail.ru",
				FromEmail:   "testCompany@mail.ru",
				Description: "inviting to our company",
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Invite.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Invite by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Invite by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved, cmpopts.IgnoreFields(invite.Invite{}, "Status")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Invite. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Invite.", dbtest.Success, testID)

			upd := invite.UpdateInviteStatus{
				Status: core2.RequestStatusApproved,
			}

			if err := core.UpdateInviteStatus(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update Invite.", dbtest.Success, testID)

			Invites, err := core.QueryByEmail(ctx, 1, 3, np.UserEmail)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Invite.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original Invite
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Status = upd.Status

			var idx int
			for i, p := range Invites {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, Invites[idx], cmpopts.IgnoreFields(invite.Invite{}, "Status")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Invite. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Invite.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Invite.", dbtest.Success, testID)

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Invite.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Invite : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Invite.", dbtest.Success, testID)
		}
	}
}
