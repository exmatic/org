package vacancy

import (
	"gitlab.com/exmatic/org/business/core/vacancy/db"
	"time"
	"unsafe"
)

type Vacancy struct {
	ID          string    `json:"vacancy_id"`
	CompanyID   string    `json:"company_id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	DateCreated time.Time `json:"date_created"`
	DateUpdated time.Time `json:"date_updated"`
}

// NewVacancy is what we require from clients when adding a Vacancy.
type NewVacancy struct {
	CompanyID   string `json:"company_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

// UpdateVacancy defines what information may be provided to modify an
// existing Vacancy. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateVacancy struct {
	Title       *string `json:"title"`
	Description *string `json:"description"`
}

// =============================================================================

func toVacancy(dbx db.Vacancy) Vacancy {
	x := (*Vacancy)(unsafe.Pointer(&dbx))
	return *x
}

func toVacancySlice(dbXX []db.Vacancy) []Vacancy {
	xx := make([]Vacancy, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toVacancy(dbx)
	}
	return xx
}
