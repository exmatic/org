package vacancy_test

import (
	"context"
	"errors"
	"fmt"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/vacancy"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/exmatic/org/business/data/dbtest"
)

var c *docker.Container

func Test_Vacancy(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := vacancy.NewCore(log, db)

	t.Log("Given the need to work with Vacancy records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single Vacancy.", testID)
		{
			ctx := context.Background()
			now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

			np := vacancy.NewVacancy{
				CompanyID:   "802be894-d1f3-11ec-baf3-526e5eed5702",
				Title:       "test title",
				Description: "test description",
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a Vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a Vacancy.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve Vacancy by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve Vacancy by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Vacancy. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Vacancy.", dbtest.Success, testID)

			upd := vacancy.UpdateVacancy{
				Title: dbtest.StringPointer("bts-digital"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update Vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update Vacancy.", dbtest.Success, testID)

			Vacancys, err := core.Query(ctx, 1, 3, np.CompanyID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated Vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated Vacancy.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original Vacancy
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Title = *upd.Title

			var idx int
			for i, p := range Vacancys {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, Vacancys[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same Vacancy. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same Vacancy.", dbtest.Success, testID)

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete Vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete Vacancy.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted Vacancy.", dbtest.Success, testID)
		}
	}
}
