// Package vacancy provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package vacancy

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/user"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/vacancy/db"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Vacancy access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Vacancy api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Vacancy to the database. It returns the created Vacancy with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewVacancy, now time.Time) (Vacancy, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Vacancy{}, err
	}

	dbx := db.Vacancy{
		ID:          validate.GenerateID(),
		CompanyID:   usr.Info.CompanyID,
		Title:       x.Title,
		Description: x.Description,
		DateCreated: now,
		DateUpdated: now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Vacancy{}, fmt.Errorf("create: %w", err)
	}

	return toVacancy(dbx), nil
}

// Update modifies data about a Vacancy. It will error if the specified ID is
// invalid or does not reference an existing Vacancy.
func (c Core) Update(ctx context.Context, VacancyID string, up UpdateVacancy) error {
	if err := validate.CheckID(VacancyID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, VacancyID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Vacancy VacancyID[%v]: %w", VacancyID, err)
	}

	if up.Title != nil {
		dbx.Title = *up.Title
	}
	if up.Description != nil {
		dbx.Description = *up.Description
	}
	dbx.DateUpdated = time.Now()

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Vacancy identified by a given ID.
func (c Core) Delete(ctx context.Context, VacancyID string) error {
	if err := validate.CheckID(VacancyID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, VacancyID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Vacancys from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Vacancy, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toVacancySlice(dbXX), nil
}

// QueryByID finds the Vacancy identified by a given ID.
func (c Core) QueryByID(ctx context.Context, VacancyID string) (Vacancy, error) {
	if err := validate.CheckID(VacancyID); err != nil {
		return Vacancy{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, VacancyID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Vacancy{}, core.ErrNotFound
		}
		return Vacancy{}, fmt.Errorf("query: %w", err)
	}

	return toVacancy(dbx), nil
}
