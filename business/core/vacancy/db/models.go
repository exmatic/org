package db

import "time"

type Vacancy struct {
	ID          string    `db:"vacancy_id"`
	CompanyID   string    `db:"company_id"`
	Title       string    `db:"title"`
	Description string    `db:"description"`
	DateCreated time.Time `db:"date_created"`
	DateUpdated time.Time `db:"date_updated"`
}
