// Package db contains vacancy related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a vacancy to the database. It returns the created vacancy with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Vacancy) error {
	const q = `
	INSERT INTO vacancy
		(vacancy_id, company_id, title, description, date_created, date_updated)
	VALUES
		(:vacancy_id, :company_id, :title, :description, :date_created, :date_updated)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting vacancy: %w", err)
	}

	return nil
}

// Update modifies data about a vacancy. It will error if the specified ID is
// invalid or does not reference an existing vacancy.
func (s Store) Update(ctx context.Context, x Vacancy) error {
	const q = `
	UPDATE
		vacancy
	SET
		"title" = :title,
		"description" = :description
	WHERE
		vacancy_id = :vacancy_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating vacancy vacancyID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the vacancy identified by a given ID.
func (s Store) Delete(ctx context.Context, vacancyID string) error {
	data := struct {
		VacancyID string `db:"vacancy_id"`
	}{
		VacancyID: vacancyID,
	}

	const q = `
	DELETE FROM
		vacancy
	WHERE
		vacancy_id = :vacancy_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting vacancy vacancyID[%v]: %w", vacancyID, err)
	}

	return nil
}

// Query gets all vacancy from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Vacancy, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM vacancy
	WHERE
		company_id = :company_id
	ORDER BY
		date_created DESC
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Vacancy
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting vacancy: %w", err)
	}

	return xx, nil
}

// QueryByID finds the vacancy identified by a given ID.
func (s Store) QueryByID(ctx context.Context, vacancyID string) (Vacancy, error) {
	data := struct {
		VacancyID string `db:"vacancy_id"`
	}{
		VacancyID: vacancyID,
	}

	const q = `
	SELECT * FROM vacancy
	WHERE
		vacancy_id = :vacancy_id`

	var x Vacancy
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Vacancy{}, fmt.Errorf("selecting vacancy vacancyID[%v]: %w", vacancyID, err)
	}

	return x, nil
}
