package grade

import (
	"gitlab.com/exmatic/org/business/core/grade/db"
	"unsafe"
)

type Grade struct {
	ID        string `json:"grade_id"`
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// NewGrade is what we require from clients when adding a Grade.
type NewGrade struct {
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// UpdateGrade defines what information may be provided to modify an
// existing Grade. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateGrade struct {
	Name *string `json:"name"`
}

// =============================================================================

func toGrade(dbx db.Grade) Grade {
	x := (*Grade)(unsafe.Pointer(&dbx))
	return *x
}

func toGradeSlice(dbXX []db.Grade) []Grade {
	xx := make([]Grade, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toGrade(dbx)
	}
	return xx
}
