package grade_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/grade"
	"gitlab.com/exmatic/org/business/data/dbtest"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
)

var c *docker.Container

func Test_grade(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := grade.NewCore(log, db)

	t.Log("Given the need to work with grade records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single grade.", testID)
		{
			ctx := context.Background()

			np := grade.NewGrade{
				CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702", // from seed
				Name:      "testgrade",
			}

			x, err := core.Create(ctx, np)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a grade.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve grade by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve grade by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same grade. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same grade.", dbtest.Success, testID)

			upd := grade.UpdateGrade{
				Name: dbtest.StringPointer("bts-digital"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update grade.", dbtest.Success, testID)

			xx, err := core.Query(ctx, 1, 3, np.CompanyID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated grade.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original grade
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Name = *upd.Name

			var idx int
			for i, p := range xx {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, xx[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same grade. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same grade.", dbtest.Success, testID)

			upd = grade.UpdateGrade{
				Name: dbtest.StringPointer("Graphic Novels"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update just some fields of grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update just some fields of grade.", dbtest.Success, testID)

			saved, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated grade.", dbtest.Success, testID)

			if saved.Name != *upd.Name {
				t.Fatalf("\t%s\tTest %d:\tShould be able to see updated Name field : got %q want %q.", dbtest.Failed, testID, saved.Name, *upd.Name)
			} else {
				t.Logf("\t%s\tTest %d:\tShould be able to see updated Name field.", dbtest.Success, testID)
			}

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete grade.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted grade : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted grade.", dbtest.Success, testID)
		}
	}
}
