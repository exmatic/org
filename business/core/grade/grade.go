// Package grade provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package grade

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/grade/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Grade access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for Grade api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a Grade to the database. It returns the created Grade with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewGrade) (Grade, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Grade{}, err
	}

	dbx := db.Grade{
		ID:        validate.GenerateID(),
		CompanyID: usr.Info.CompanyID,
		Name:      x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Grade{}, fmt.Errorf("create: %w", err)
	}

	return toGrade(dbx), nil
}

// Update modifies data about a Grade. It will error if the specified ID is
// invalid or does not reference an existing Grade.
func (c Core) Update(ctx context.Context, GradeID string, up UpdateGrade) error {
	if err := validate.CheckID(GradeID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, GradeID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Grade GradeID[%v]: %w", GradeID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the Grade identified by a given ID.
func (c Core) Delete(ctx context.Context, GradeID string) error {
	if err := validate.CheckID(GradeID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, GradeID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Grade from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Grade, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toGradeSlice(dbXX), nil
}

// QueryByID finds the Grade identified by a given ID.
func (c Core) QueryByID(ctx context.Context, GradeID string) (Grade, error) {
	if err := validate.CheckID(GradeID); err != nil {
		return Grade{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, GradeID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Grade{}, core.ErrNotFound
		}
		return Grade{}, fmt.Errorf("query: %w", err)
	}

	return toGrade(dbx), nil
}
