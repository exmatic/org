// Package db contains grade related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a grade to the database. It returns the created grade with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Grade) error {
	const q = `
	INSERT INTO grades
		(grade_id, company_id, name)
	VALUES
		(:grade_id, :company_id, :name)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting grade: %w", err)
	}

	return nil
}

// Update modifies data about a grade. It will error if the specified ID is
// invalid or does not reference an existing grade.
func (s Store) Update(ctx context.Context, x Grade) error {
	const q = `
	UPDATE
		grades
	SET
		"name" = :name
	WHERE
		grade_id = :grade_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating grade gradeID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the grade identified by a given ID.
func (s Store) Delete(ctx context.Context, gradeID string) error {
	data := struct {
		GradeID string `db:"grade_id"`
	}{
		GradeID: gradeID,
	}

	const q = `
	DELETE FROM
		grades
	WHERE
		grade_id = :grade_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting grade gradeID[%v]: %w", gradeID, err)
	}

	return nil
}

// Query gets all grade from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]Grade, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM grades
	WHERE
		company_id = :company_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []Grade
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting grade: %w", err)
	}

	return xx, nil
}

// QueryByID finds the grade identified by a given ID.
func (s Store) QueryByID(ctx context.Context, gradeID string) (Grade, error) {
	data := struct {
		GradeID string `db:"grade_id"`
	}{
		GradeID: gradeID,
	}

	const q = `
	SELECT * FROM grades
	WHERE
		grade_id = :grade_id`

	var x Grade
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return Grade{}, fmt.Errorf("selecting grade gradeID[%v]: %w", gradeID, err)
	}

	return x, nil
}
