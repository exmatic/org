package db

type Grade struct {
	ID        string `db:"grade_id"`
	CompanyID string `db:"company_id"`
	Name      string `db:"name"`
}
