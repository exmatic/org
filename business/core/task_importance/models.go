package task_importance

import (
	"gitlab.com/exmatic/org/business/core/task_importance/db"
	"unsafe"
)

type TaskImportance struct {
	ID        string `json:"request_type_id"`
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// NewTaskImportance is what we require from clients when adding a TaskImportance.
type NewTaskImportance struct {
	CompanyID string `json:"company_id"`
	Name      string `json:"name"`
}

// UpdateTaskImportance defines what information may be provided to modify an
// existing TaskImportance. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateTaskImportance struct {
	Name *string `json:"name"`
}

// =============================================================================

func toTaskImportance(dbx db.TaskImportance) TaskImportance {
	x := (*TaskImportance)(unsafe.Pointer(&dbx))
	return *x
}

func toTaskImportanceSlice(dbXX []db.TaskImportance) []TaskImportance {
	xx := make([]TaskImportance, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toTaskImportance(dbx)
	}
	return xx
}
