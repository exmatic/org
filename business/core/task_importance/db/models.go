package db

type TaskImportance struct {
	ID        string `db:"task_importance_id"`
	CompanyID string `db:"company_id"`
	Name      string `db:"name"`
}
