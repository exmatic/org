// Package db contains task_importance related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// Create adds a task_importance to the database. It returns the created task_importance with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x TaskImportance) error {
	const q = `
	INSERT INTO task_importance
		(task_importance_id, company_id, name)
	VALUES
		(:task_importance_id, :company_id, :name)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting task_importance: %w", err)
	}

	return nil
}

// Update modifies data about a task_importance. It will error if the specified ID is
// invalid or does not reference an existing task_importance.
func (s Store) Update(ctx context.Context, x TaskImportance) error {
	const q = `
	UPDATE
		task_importance
	SET
		"name" = :name
	WHERE
		task_importance_id = :task_importance_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, x); err != nil {
		return fmt.Errorf("updating task_importance task_importanceID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the task_importance identified by a given ID.
func (s Store) Delete(ctx context.Context, TaskImportanceID string) error {
	data := struct {
		TaskImportanceID string `db:"task_importance_id"`
	}{
		TaskImportanceID: TaskImportanceID,
	}

	const q = `
	DELETE FROM
		task_importance
	WHERE
		task_importance_id = :task_importance_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting task_importance request_typeID[%v]: %w", TaskImportanceID, err)
	}

	return nil
}

// Query gets all task_importance from the database.
func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]TaskImportance, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
	}{
		CompanyID:   companyID,
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT * FROM task_importance
	WHERE
		company_id = :company_id
	ORDER BY
		name
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`

	var xx []TaskImportance
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting task_importance: %w", err)
	}

	return xx, nil
}

// QueryByID finds the task_importance identified by a given ID.
func (s Store) QueryByID(ctx context.Context, TaskImportanceID string) (TaskImportance, error) {
	data := struct {
		TaskImportanceID string `db:"task_importance_id"`
	}{
		TaskImportanceID: TaskImportanceID,
	}

	const q = `
	SELECT * FROM task_importance
	WHERE
		task_importance_id = :task_importance_id`

	var x TaskImportance
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &x); err != nil {
		return TaskImportance{}, fmt.Errorf("selecting task_importance task_importanceID[%v]: %w", TaskImportanceID, err)
	}

	return x, nil
}
