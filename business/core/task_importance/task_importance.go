// Package task_importance provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package task_importance

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/task_importance/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for TaskImportance access.
type Core struct {
	store db.Store
}

// NewCore constructs a core for TaskImportance api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

// Create adds a TaskImportance to the database. It returns the created TaskImportance with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewTaskImportance) (TaskImportance, error) {
	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return TaskImportance{}, err
	}

	dbx := db.TaskImportance{
		ID:        validate.GenerateID(),
		CompanyID: usr.Info.CompanyID,
		Name:      x.Name,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return TaskImportance{}, fmt.Errorf("create: %w", err)
	}

	return toTaskImportance(dbx), nil
}

// Update modifies data about a TaskImportance. It will error if the specified ID is
// invalid or does not reference an existing TaskImportance.
func (c Core) Update(ctx context.Context, TaskImportanceID string, up UpdateTaskImportance) error {
	if err := validate.CheckID(TaskImportanceID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskImportanceID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating TaskImportance TaskImportanceID[%v]: %w", TaskImportanceID, err)
	}

	if up.Name != nil {
		dbx.Name = *up.Name
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

// Delete removes the TaskImportance identified by a given ID.
func (c Core) Delete(ctx context.Context, TaskImportanceID string) error {
	if err := validate.CheckID(TaskImportanceID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, TaskImportanceID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all TaskImportances from the database.
func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int, companyID string) ([]TaskImportance, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	dbXX, err := c.store.Query(ctx, pageNumber, rowsPerPage, companyID)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toTaskImportanceSlice(dbXX), nil
}

// QueryByID finds the TaskImportance identified by a given ID.
func (c Core) QueryByID(ctx context.Context, TaskImportanceID string) (TaskImportance, error) {
	if err := validate.CheckID(TaskImportanceID); err != nil {
		return TaskImportance{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, TaskImportanceID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return TaskImportance{}, core.ErrNotFound
		}
		return TaskImportance{}, fmt.Errorf("query: %w", err)
	}

	return toTaskImportance(dbx), nil
}
