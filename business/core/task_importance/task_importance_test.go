package task_importance_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/task_importance"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/data/dbtest"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"
)

var c *docker.Container

func Test_TaskImportance(t *testing.T) {
	// start db in container
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	core := task_importance.NewCore(log, db)

	t.Log("Given the need to work with TaskImportance records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single TaskImportance.", testID)
		{
			ctx := context.WithValue(context.Background(), "user", &user.User{
				ID:        "628a47ac1b19204b0e831722",
				Profile:   user.Profile{Email: "asdtest@gmail.com"},
				Info:      &user.Info{CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702"},
				Role:      "ROLE_ADMIN",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			})

			np := task_importance.NewTaskImportance{
				CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702", // from seed
				Name:      "testTaskImportance",
			}

			x, err := core.Create(ctx, np)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a TaskImportance.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve TaskImportance by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve TaskImportance by ID.", dbtest.Success, testID)

			if diff := cmp.Diff(x, saved); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same TaskImportance. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same TaskImportance.", dbtest.Success, testID)

			upd := task_importance.UpdateTaskImportance{
				Name: dbtest.StringPointer("bts-digital"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update TaskImportance.", dbtest.Success, testID)

			TaskImportances, err := core.Query(ctx, 1, 3, np.CompanyID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated TaskImportance.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original TaskImportance
			// and change just the fields we expect then diff it with what was saved.
			want := x
			want.Name = *upd.Name

			var idx int
			for i, p := range TaskImportances {
				if p.ID == want.ID {
					idx = i
				}
			}
			if diff := cmp.Diff(want, TaskImportances[idx]); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same TaskImportance. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same TaskImportance.", dbtest.Success, testID)

			upd = task_importance.UpdateTaskImportance{
				Name: dbtest.StringPointer("Graphic Novels"),
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update just some fields of TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update just some fields of TaskImportance.", dbtest.Success, testID)

			saved, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated TaskImportance.", dbtest.Success, testID)

			if saved.Name != *upd.Name {
				t.Fatalf("\t%s\tTest %d:\tShould be able to see updated Name field : got %q want %q.", dbtest.Failed, testID, saved.Name, *upd.Name)
			} else {
				t.Logf("\t%s\tTest %d:\tShould be able to see updated Name field.", dbtest.Success, testID)
			}

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete TaskImportance.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted TaskImportance : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted TaskImportance.", dbtest.Success, testID)
		}
	}
}
