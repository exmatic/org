package request_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-cmp/cmp/cmpopts"
	core2 "gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/request"
	reqdb "gitlab.com/exmatic/org/business/core/request/db"
	"gitlab.com/exmatic/org/business/core/user"
	"gitlab.com/exmatic/org/business/core/vacation"
	"gitlab.com/exmatic/org/business/sys/filter"
	"gitlab.com/exmatic/org/foundation/docker"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/exmatic/org/business/data/dbtest"
)

var c *docker.Container

func Test_request(t *testing.T) {
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	log, db, teardown := dbtest.NewUnit(t, c, "organization")
	t.Cleanup(teardown)

	//mailer := mailer.New("smtp.mailtrap.io", 2525, "2985b09edba520", "302520886b0aa1", "Exmatic[No Reply] <no-reply@inbox.mailtrap.io>")
	//authStore := userdb.NewStore(&http.Client{})

	core := request.NewCore(log, db, nil, vacation.NewCore(log, db), nil)

	t.Log("Given the need to work with request records.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single request.", testID)
		{
			ctx := context.Background()
			now := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)

			np := request.NewRequest{
				CompanyID:     "802be894-d1f3-11ec-baf3-526e5eed5702",
				DepartmentID:  "802bec54-d1f3-11ec-baf3-526e5eed5702",
				RequestTypeID: "802bec86-d1f3-11ec-baf3-526e5eed5702",
				UserID:        "628aca22a2394534339d7d21",
				Description:   "test description",
				DateSince:     time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
				DateTo:        time.Date(2019, time.January, 2, 0, 0, 0, 0, time.UTC),
			}

			x, err := core.Create(ctx, np, now)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create a request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create a request.", dbtest.Success, testID)

			saved, err := core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve request by ID: %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve request by ID.", dbtest.Success, testID)

			xToJoinReq := request.JoinRequest{
				Request:         x,
				RequestTypeName: saved.RequestTypeName,
				CompanyName:     saved.CompanyName,
			}

			if diff := cmp.Diff(xToJoinReq, saved, cmpopts.IgnoreFields(request.JoinRequest{}, "RequestStatus")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same request.\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same request.", dbtest.Success, testID)

			dateSince2 := time.Date(2019, time.January, 5, 1, 1, 1, 0, time.UTC)
			dateTo2 := time.Date(2019, time.January, 6, 1, 1, 1, 0, time.UTC)
			upd := request.UpdateRequest{
				Description: dbtest.StringPointer("updated test description"),
				DateSince:   &dateSince2,
				DateTo:      &dateTo2,
			}

			if err := core.Update(ctx, x.ID, upd); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update request.", dbtest.Success, testID)

			requests, err := core.Query(ctx, np.CompanyID, filter.Filters{
				PageNumber:   1,
				RowsPerPage:  3,
				Sort:         "date_created",
				SortSafeList: []string{"date_created", "-date_created"},
			}, reqdb.FilterRequest{})
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated request.", dbtest.Success, testID)

			// Check specified fields were updated. Make a copy of the original request
			// and change just the fields we expect then diff it with what was saved.
			want := xToJoinReq
			want.Description = *upd.Description
			want.DateSince = *upd.DateSince
			want.DateTo = *upd.DateTo

			var idx int
			for i, p := range requests {
				if p.ID == want.ID {
					idx = i
				}
			}

			if diff := cmp.Diff(want, requests[idx], cmpopts.IgnoreFields(request.JoinRequest{}, "RequestStatus", "CompanyName")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back the same request. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back the same request.", dbtest.Success, testID)

			upd2 := request.UpdateRequestStatus{
				RequestStatus: core2.RequestStatusApproved,
			}

			usr := user.User{
				ID:        "628bad573c3dbc57eeff69a8",
				Profile:   user.Profile{},
				Info:      &user.Info{CompanyID: "802be894-d1f3-11ec-baf3-526e5eed5702"},
				Role:      "ROLE_EMPLOYEE",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			}

			ctx = context.WithValue(ctx, "user", &usr)

			tokens := []string{"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4YmFkNTczYzNkYmM1N2VlZmY2OWE4Iiwicm9sZSI6IlJPTEVfRU1QTE9ZRUUiLCJleHAiOjE2NTMzMzIzODF9.pyqww9bCAz4fSUn4Bonvl2NjZzSC467GryE4bdX0HZo",
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjI4YmFkNTczYzNkYmM1N2VlZmY2OWE4IiwiZXhwIjoxNjU1OTIzNDgxLCJjb3VudGVyIjo1fQ.-9skxahMz0Vz6xVHHSv9IZ4V74AHmXWj2bdcuh25Fh8"}

			ctx = context.WithValue(ctx, "tokens", tokens)

			vacID, err := core.UpdateRequestStatus(ctx, x.ID, upd2)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to update request status : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to update just request status.", dbtest.Success, testID)

			if vacID == "" {
				t.Fatalf("\t%s\tTest %d:\tShould be able to create vacancy and retrieve vacancyID : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to create vacancy and retrieve vacancyID.", dbtest.Success, testID)

			saved, err = core.QueryByID(ctx, x.ID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve updated request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve updated request.", dbtest.Success, testID)

			if saved.RequestStatus != upd2.RequestStatus {
				t.Fatalf("\t%s\tTest %d:\tShould be able to see updated RequestStatus field : got %q want %q.", dbtest.Failed, testID, saved.Description, *upd.Description)
			} else {
				t.Logf("\t%s\tTest %d:\tShould be able to see updated RequestStatus field.", dbtest.Success, testID)
			}

			expVac := vacation.JoinVacation{
				Vacation: vacation.Vacation{
					CompanyID:     saved.CompanyID,
					DepartmentID:  saved.DepartmentID,
					RequestTypeID: saved.RequestTypeID,
					UserID:        saved.UserID,
					DateSince:     saved.DateSince,
					DateTo:        saved.DateTo,
				},
				RequestTypeName: saved.RequestTypeName,
			}

			vac, err := core.VacCore.QueryByID(ctx, vacID)
			if err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to retrieve created vacancy : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to retrieve created vacancy.", dbtest.Success, testID)

			if diff := cmp.Diff(expVac, vac, cmpopts.IgnoreFields(vacation.Vacation{}, "ID")); diff != "" {
				t.Fatalf("\t%s\tTest %d:\tShould get back expected vacancy. Diff:\n%s", dbtest.Failed, testID, diff)
			}
			t.Logf("\t%s\tTest %d:\tShould get back expected vacancy.", dbtest.Success, testID)

			if err := core.Delete(ctx, x.ID); err != nil {
				t.Fatalf("\t%s\tTest %d:\tShould be able to delete request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould be able to delete request.", dbtest.Success, testID)

			_, err = core.QueryByID(ctx, x.ID)
			if !errors.Is(err, core2.ErrNotFound) {
				t.Fatalf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted request : %s.", dbtest.Failed, testID, err)
			}
			t.Logf("\t%s\tTest %d:\tShould NOT be able to retrieve deleted request.", dbtest.Success, testID)
		}
	}
}
