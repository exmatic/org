package db

import (
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/sys/validate"
	"time"
)

type Request struct {
	ID              string    `db:"request_id"`
	CompanyID       string    `db:"company_id"`
	DepartmentID    string    `db:"department_id"`
	RequestTypeID   string    `db:"request_type_id"`
	RequestStatus   string    `db:"request_status"` // postgres enum
	UserID          string    `db:"user_id"`
	StatusChangedBy string    `db:"status_changed_by"`
	Description     string    `db:"description"`
	DateSince       time.Time `db:"date_since"`
	DateTo          time.Time `db:"date_to"`
	DateCreated     time.Time `db:"date_created"`
}

type JoinRequest struct {
	Request
	RequestType string `db:"request_type_name"`
	CompanyName string `db:"company_name"`
}

type FilterRequest struct {
	DepartmentID  string `db:"department_id"`
	RequestTypeID string `db:"request_type_id"`
	RequestStatus string `db:"request_status"`
	UserID        string `db:"user_id"`
}

func ValidateFilterRequest(v *validate.Validator, fr FilterRequest) {
	if fr.DepartmentID != "" {
		if err := validate.CheckID(fr.DepartmentID); err != nil {
			v.Check(false, "department_id", err.Error())
		}
	}
	if fr.RequestTypeID != "" {
		if err := validate.CheckID(fr.RequestTypeID); err != nil {
			v.Check(false, "request_type_id", err.Error())
		}
	}
	if fr.RequestStatus != "" {
		v.Check(core.IsSupportedRequestStatus(fr.RequestStatus), "request_status", core.ErrUnsupportedRequestStatus.Error())
	}
}
