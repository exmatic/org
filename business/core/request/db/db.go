// Package db contains request related CRUD functionality.
package db

import (
	"context"
	"fmt"
	"gitlab.com/exmatic/org/business/sys/filter"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of APIs for user access.
type Store struct {
	Log *zap.SugaredLogger
	db  sqlx.ExtContext
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		Log: log,
		db:  db,
	}
}

// Create adds a request to the database. It returns the created request with
// fields like ID and DateCreated populated.
func (s Store) Create(ctx context.Context, x Request) error {
	const q = `
	INSERT INTO request
		(request_id, company_id, department_id, request_type_id, user_id, status_changed_by, description, date_since, date_to, date_created)
	VALUES
		(:request_id, :company_id, :department_id, :request_type_id, :user_id, :status_changed_by, :description, :date_since, :date_to, :date_created)`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("inserting request: %w", err)
	}

	return nil
}

// Update modifies data about a request. It will error if the specified ID is
// invalid or does not reference an existing request.
func (s Store) Update(ctx context.Context, x JoinRequest) error {
	const q = `
	UPDATE
		request
	SET
		"description" = :description,
		"date_since" = :date_since,
		"date_to" = :date_to
	WHERE
		request_id = :request_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("updating request requestID[%v]: %w", x.ID, err)
	}

	return nil
}

func (s Store) UpdateRequestStatus(ctx context.Context, x JoinRequest) error {
	const q = `
	UPDATE
		request
	SET
		"request_status" = :request_status,
		"status_changed_by" = :status_changed_by
	WHERE
		request_id = :request_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, x); err != nil {
		return fmt.Errorf("updating request requestID[%v]: %w", x.ID, err)
	}

	return nil
}

// Delete removes the request identified by a given ID.
func (s Store) Delete(ctx context.Context, requestID string) error {
	data := struct {
		RequestID string `db:"request_id"`
	}{
		RequestID: requestID,
	}

	const q = `
	DELETE FROM
		request
	WHERE
		request_id = :request_id`

	if err := database.NamedExecContext(ctx, s.Log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting request requestID[%v]: %w", requestID, err)
	}

	return nil
}

// Query gets all request from the database.
func (s Store) Query(ctx context.Context, companyID string, filters filter.Filters, filterRequest FilterRequest) ([]JoinRequest, error) {
	data := struct {
		CompanyID   string `db:"company_id"`
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		FilterRequest
	}{
		CompanyID:     companyID,
		Offset:        (filters.PageNumber - 1) * filters.RowsPerPage,
		RowsPerPage:   filters.RowsPerPage,
		FilterRequest: filterRequest,
	}

	q := fmt.Sprintf(`
	SELECT 
		r.*,
		rt.name as request_type_name
	FROM 
		request as r
	JOIN 
		request_types as rt on r.request_type_id = rt.request_type_id
	WHERE
		(r.company_id = :company_id) AND
		(
			(:department_id = '' OR CAST(r.department_id as text) = :department_id) AND
			(:request_type_id = '' OR CAST(r.request_type_id as text) = :request_type_id) AND
			(:request_status = '' OR CAST(r.request_status as text) = :request_status) AND
			(:user_id = '' OR r.user_id = :user_id)
		)
	ORDER BY
		r.%s %s
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY`,
		filters.SortColumn(), filters.SortDirection())

	var xx []JoinRequest
	if err := database.NamedQuerySlice(ctx, s.Log, s.db, q, data, &xx); err != nil {
		return nil, fmt.Errorf("selecting request: %w", err)
	}

	return xx, nil
}

// QueryByID finds the request identified by a given ID.
func (s Store) QueryByID(ctx context.Context, requestID string) (JoinRequest, error) {
	data := struct {
		RequestID string `db:"request_id"`
	}{
		RequestID: requestID,
	}

	const q = `
	SELECT 
		r.*,
		rt.name as request_type_name,
		c.name as company_name
	FROM 
		request as r
	JOIN request_types as rt on r.request_type_id = rt.request_type_id
	JOIN company as c on r.company_id = c.company_id
	WHERE 
		r.request_id = :request_id`

	var x JoinRequest
	if err := database.NamedQueryStruct(ctx, s.Log, s.db, q, data, &x); err != nil {
		return JoinRequest{}, fmt.Errorf("selecting request requestID[%v]: %w", requestID, err)
	}

	return x, nil
}
