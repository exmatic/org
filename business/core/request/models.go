package request

import (
	"gitlab.com/exmatic/org/business/core/request/db"
	"time"
	"unsafe"
)

type Request struct {
	ID            string `json:"request_id"`
	CompanyID     string `json:"company_id"`
	DepartmentID  string `json:"department_id"`
	RequestTypeID string `json:"request_type_id"`
	RequestStatus string `json:"request_status"` // postgres enum
	UserID        string `json:"user_id"`
	// user id of the one who approves/declines request
	StatusChangedBy string    `json:"status_changed_by"`
	Description     string    `json:"description"`
	DateSince       time.Time `json:"date_since"`
	DateTo          time.Time `json:"date_to"`
	DateCreated     time.Time `json:"date_created"`
}

type JoinRequest struct {
	Request
	RequestTypeName string `json:"request_type_name"`
	CompanyName     string `json:"company_name,omitempty"`
}

// NewRequest is what we require from clients when adding a Request.
type NewRequest struct {
	CompanyID     string    `json:"company_id"`
	DepartmentID  string    `json:"department_id"`
	RequestTypeID string    `json:"request_type_id"`
	UserID        string    `json:"user_id"`
	Description   string    `json:"description"`
	DateSince     time.Time `json:"date_since"`
	DateTo        time.Time `json:"date_to"`
}

// UpdateRequest defines what information may be provided to modify an
// existing Request. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateRequest struct {
	Description *string    `json:"description"`
	DateSince   *time.Time `json:"date_since"`
	DateTo      *time.Time `json:"date_to"`
}

type UpdateRequestStatus struct {
	StatusChangedBy string `json:"status_changed_by"`
	RequestStatus   string `json:"request_status"`
}

// =============================================================================

func toRequest(dbx db.Request) Request {
	x := (*Request)(unsafe.Pointer(&dbx))
	return *x
}

func toJoinRequest(dbx db.JoinRequest) JoinRequest {
	x := (*JoinRequest)(unsafe.Pointer(&dbx))
	return *x
}

func toRequestSlice(dbXX []db.Request) []Request {
	xx := make([]Request, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toRequest(dbx)
	}
	return xx
}

func toJoinRequestSlice(dbXX []db.JoinRequest) []JoinRequest {
	xx := make([]JoinRequest, len(dbXX))
	for i, dbx := range dbXX {
		xx[i] = toJoinRequest(dbx)
	}
	return xx
}
