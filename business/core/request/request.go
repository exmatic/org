// Package request provides an example of a core business API. Right now these
// calls are just wrapping the data/store layer. But at some point you will
// want auditing or something that isn't specific to the data/store layer.
package request

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/exmatic/org/business/core"
	"gitlab.com/exmatic/org/business/core/user"
	userdb "gitlab.com/exmatic/org/business/core/user/db"
	"gitlab.com/exmatic/org/business/mailer"
	"gitlab.com/exmatic/org/business/sys/filter"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/exmatic/org/business/core/request/db"
	"gitlab.com/exmatic/org/business/core/vacation"
	"gitlab.com/exmatic/org/business/sys/database"
	"gitlab.com/exmatic/org/business/sys/validate"
	"go.uber.org/zap"
)

// Core manages the set of APIs for Request access.
type Core struct {
	store  db.Store
	mailer *mailer.Mailer

	VacCore   vacation.Core
	authStore *userdb.AuthStore
}

// NewCore constructs a core for Request api access.
func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB, mailer *mailer.Mailer, vacCore vacation.Core, as *userdb.AuthStore) Core {
	return Core{
		store:     db.NewStore(log, sqlxDB),
		VacCore:   vacCore,
		mailer:    mailer,
		authStore: as,
	}
}

// Create adds a Request to the database. It returns the created Request with
// fields like ID and DateCreated populated.
func (c Core) Create(ctx context.Context, x NewRequest, now time.Time) (Request, error) {

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return Request{}, err
	}

	dbx := db.Request{
		ID:              validate.GenerateID(),
		CompanyID:       usr.Info.CompanyID,
		DepartmentID:    x.DepartmentID,
		RequestTypeID:   x.RequestTypeID,
		UserID:          usr.ID,
		Description:     x.Description,
		DateSince:       x.DateSince,
		DateTo:          x.DateTo,
		StatusChangedBy: "",
		DateCreated:     now,
	}

	if err := c.store.Create(ctx, dbx); err != nil {
		return Request{}, fmt.Errorf("create: %w", err)
	}

	return toRequest(dbx), nil
}

// Update modifies data about a Request. It will error if the specified ID is
// invalid or does not reference an existing Request.
func (c Core) Update(ctx context.Context, RequestID string, up UpdateRequest) error {
	if err := validate.CheckID(RequestID); err != nil {
		return core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, RequestID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return core.ErrNotFound
		}
		return fmt.Errorf("updating Request RequestID[%v]: %w", RequestID, err)
	}

	if up.Description != nil {
		dbx.Description = *up.Description
	}
	if up.DateSince != nil {
		dbx.DateSince = *up.DateSince
	}
	if up.DateTo != nil {
		dbx.DateTo = *up.DateTo
	}

	if err := c.store.Update(ctx, dbx); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

func (c Core) UpdateRequestStatus(ctx context.Context, RequestID string, up UpdateRequestStatus) (vacationID string, err error) {
	if err := validate.CheckID(RequestID); err != nil {
		return vacationID, core.ErrInvalidID
	}

	if ok := core.IsSupportedRequestStatus(up.RequestStatus); !ok {
		return vacationID, core.ErrUnsupportedRequestStatus
	}

	dbx, err := c.store.QueryByID(ctx, RequestID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return vacationID, core.ErrNotFound
		}
		return vacationID, fmt.Errorf("updating Request RequestID[%v]: %w", RequestID, err)
	}

	usr, err := user.UserFromContext(ctx)
	if err != nil {
		return vacationID, err
	}

	dbx.StatusChangedBy = usr.ID
	dbx.RequestStatus = up.RequestStatus

	if err = c.store.UpdateRequestStatus(ctx, dbx); err != nil {
		return vacationID, fmt.Errorf("update: %w", err)
	}

	if up.RequestStatus == core.RequestStatusInProgress {
		return vacationID, nil
	}

	if c.mailer != nil {
		fetchedUsr, err := c.authStore.FetchByID(ctx, dbx.UserID)
		if err != nil {
			return vacationID, err
		}

		usrEmail := fetchedUsr.Profile.Email
		usrName := fetchedUsr.Profile.Username

		// send email
		core.Background(c.store.Log, func() {
			data := map[string]interface{}{
				"request_id":     dbx.Request.ID,
				"request_status": up.RequestStatus,
				"user_name":      usrName,
				"company_name":   dbx.CompanyName,
			}

			if err = c.mailer.Send(usrEmail, "request_status_change.tmpl", data); err != nil {
				c.store.Log.Errorw("mailer", "ERROR", err)
			} else {
				c.store.Log.Infow("mailer", "email send for userID", dbx.UserID, "requestID", dbx.Request.ID)
			}
		})
	}

	if up.RequestStatus != core.RequestStatusApproved {
		return vacationID, nil
	}

	// create vacation

	nx := vacation.NewVacation{
		CompanyID:     dbx.CompanyID,
		DepartmentID:  dbx.DepartmentID,
		RequestTypeID: dbx.RequestTypeID,
		UserID:        dbx.UserID,
		DateSince:     dbx.DateSince,
		DateTo:        dbx.DateTo,
	}

	vac, err := c.VacCore.Create(ctx, nx)
	if err != nil {
		return vacationID, fmt.Errorf("creating new Vacation, nx[%+v]: %w", nx, err)
	}

	return vac.ID, nil
}

// Delete removes the Request identified by a given ID.
func (c Core) Delete(ctx context.Context, RequestID string) error {
	if err := validate.CheckID(RequestID); err != nil {
		return core.ErrInvalidID
	}

	if err := c.store.Delete(ctx, RequestID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

// Query gets all Requests from the database.
func (c Core) Query(ctx context.Context, companyID string, filters filter.Filters, filterRequest db.FilterRequest) ([]JoinRequest, error) {
	if err := validate.CheckID(companyID); err != nil {
		return nil, core.ErrInvalidID
	}

	// Initialize a new Validator instance.
	v := validate.New()

	if db.ValidateFilterRequest(v, filterRequest); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	if filter.ValidateFilters(v, filters); !v.Valid() {
		return nil, validate.ToFilterErrors(v.Errors)
	}

	dbXX, err := c.store.Query(ctx, companyID, filters, filterRequest)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toJoinRequestSlice(dbXX), nil
}

// QueryByID finds the Request identified by a given ID.
func (c Core) QueryByID(ctx context.Context, RequestID string) (JoinRequest, error) {
	if err := validate.CheckID(RequestID); err != nil {
		return JoinRequest{}, core.ErrInvalidID
	}

	dbx, err := c.store.QueryByID(ctx, RequestID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return JoinRequest{}, core.ErrNotFound
		}
		return JoinRequest{}, fmt.Errorf("query: %w", err)
	}

	return toJoinRequest(dbx), nil
}
