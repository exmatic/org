alter table department
    drop constraint if exists department_company_id_fkey;

alter table project
    drop constraint if exists project_department_id_fkey;

alter table team
    drop constraint if exists team_project_id_fkey;

alter table positions
    drop constraint if exists positions_company_id_fkey;

alter table grades
    drop constraint if exists grades_company_id_fkey;

alter table request_statuses
    drop constraint if exists request_statuses_company_id_fkey;

alter table request_types
    drop constraint if exists request_types_company_id_fkey;

alter table request
    drop constraint if exists request_company_id_fkey,
    drop constraint if exists request_department_id_fkey,
    drop constraint if exists request_request_type_id_fkey,
    drop constraint if exists request_request_status_fkey;

alter table vacations
    drop constraint if exists vacations_request_type_id_fkey,
    drop constraint if exists vacations_company_id_fkey,
    drop constraint if exists vacations_department_id_fkey;

alter table complaints
    drop constraint if exists complaints_company_id_fkey,
    drop constraint if exists complaints_department_id_fkey;

alter table task_importance
    drop constraint if exists task_importance_company_id_fkey;

alter table task_status
    drop constraint if exists task_status_company_id_fkey;

alter table tasks
    drop constraint if exists tasks_company_id_fkey,
    drop constraint if exists tasks_department_id_fkey,
    drop constraint if exists tasks_project_id_fkey,
    drop constraint if exists tasks_team_id_fkey,
    drop constraint if exists tasks_task_importance_id_fkey,
    drop constraint if exists tasks_task_status_id_fkey;

alter table comments
    drop constraint if exists comments_company_id_fkey,
    drop constraint if exists comments_task_id_fkey;

alter table vacancy
    drop constraint if exists vacancy_company_id_fkey;

alter table candidate
    drop constraint if exists candidate_company_id_fkey,
    drop constraint if exists candidate_vacancy_id_fkey;

