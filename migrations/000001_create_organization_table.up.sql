-- Version: 1.4
-- Description: Create table company
CREATE TABLE company
(
    company_id      UUID PRIMARY KEY,
    owner_id        text,
    name            TEXT        NOT NULL,
    phone           VARCHAR(12),
    email           TEXT UNIQUE NOT NULL,
    address         TEXT,
    bik             TEXT,
    website         TEXT,
    date_created    TIMESTAMP,
    date_registered TIMESTAMP
);

-- Version: 1.5
-- Description: Create table department
CREATE TABLE department
(
    department_id          UUID PRIMARY KEY,
    company_id             UUID,
    department_director_id TEXT,
    name                   TEXT,
    is_projectless         boolean
);

-- Version: 1.6
-- Description: Create table project
CREATE TABLE project
(
    project_id         UUID PRIMARY KEY,
    department_id      UUID,
    project_manager_id text,
    name               TEXT,
    date_created       TIMESTAMP
);

-- Version: 1.7
-- Description: Create table team
CREATE TABLE team
(
    team_id      UUID PRIMARY KEY,
    project_id   UUID,
    team_lead_id text,
    name         TEXT NOT NULL
);

-- Version: 1.8
-- Description: Create table positions
CREATE TABLE positions
(
    position_id UUID primary key,
    company_id  UUID,
    name        TEXT not null
);

-- Version: 1.9
-- Description: Create table grades
CREATE TABLE grades
(
    grade_id   UUID primary key,
    company_id UUID,
    name       TEXT
);

create type request_statuses_e as enum ('in progress', 'declined', 'approved');

-- Version: 1.12
-- Description: Create table company
create table request_types
(
    request_type_id UUID primary key,
    company_id      UUID,
    name            text
);

-- Version: 1.13
-- Description: Create table company
create table request
(
    request_id        UUID primary key,
    company_id        UUID,
    department_id     UUID,
    request_type_id   UUID,
    request_status    request_statuses_e default 'in progress',
    user_id           TEXT,
    status_changed_by text,
    description       text,
    date_since        timestamp, -- may be null for non vacation request, optional
    date_to           timestamp, -- may be null for non vacation request, optional
    date_created      timestamp
);

-- Version: 1.14
-- Description: Create table company
create table vacations
(
    vacation_id     UUID primary key,
    company_id      UUID,
    department_id   UUID,
    request_type_id UUID,
    user_id         TEXT,
    date_since      timestamp,
    date_to         timestamp
);

-- Version: 1.15
-- Description: Create table company
create table complaints
(
    complaint_id  UUID PRIMARY KEY,
    company_id    UUID,
    department_id UUID,
    created_by    TEXT,
    data          text,
    is_anonymous  boolean,
    date_created  timestamp
);

-- tasks

-- Version: 1.16
-- Description: Create table task_importance
create table task_importance
(
    task_importance_id UUID primary key,
    company_id         UUID,
    name               text not null
);

-- Version: 1.17
-- Description: Create table task_status
create table task_status
(
    task_status_id       UUID PRIMARY KEY,
    company_id           UUID,
    percentage_completed int,
    name                 text not null
);

-- Version: 1.18
-- Description: Create table tasks
create table tasks
(
    task_id            UUID PRIMARY KEY,
    company_id         UUID,
    department_id      UUID,
    project_id         UUID,
    team_id            UUID,
    assigned_by        TEXT NOT NULL,
    assigned_to        TEXT,   -- can be null
    title              text NOT NULL,
    description        text,   -- can be null
    task_importance_id UUID,   -- default should be "normal"
    task_status_id     UUID,   -- default should be "0% not started"
    date_created       timestamp,
    date_updated       timestamp
);

-- Version: 1.19
-- Description: Create table company
create table comments
(
    comment_id UUID PRIMARY key,
    company_id UUID,
    task_id    UUID,
    user_id    text,
    data       text
);

-- Version: 1.21
-- Description: Create table company
create table vacancy
(
    vacancy_id   UUID PRIMARY KEY,
    company_id   UUID,
    title        text,
    description  text,
    date_created timestamp,
    date_updated timestamp
);

-- Version: 1.22
-- Description: Create table candidate
create table candidate
(
    candidate_id UUID PRIMARY KEY,
    company_id   UUID,
    creator_id   TEXT not null,
    vacancy_id   UUID,  -- may be null
    cv           bytea, -- pdf encoded to bytea
    skills       text[],
    notes        text,
    date_created timestamp,
    date_updated timestamp
);

create table invites
(
    invite_id    uuid primary key,
    company_id   uuid,
    user_email   text,
    from_email   text,
    description  text,
    status       request_statuses_e default 'in progress',
    date_created timestamp
);
