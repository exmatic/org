-- Version: 1.23
-- Description: Create table candidate
alter table department
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.24
-- Description: Create table candidate
alter table project
    add FOREIGN KEY (department_id) REFERENCES department (department_id);

-- Version: 1.25
-- Description: Create table candidate
alter table team
    add FOREIGN KEY (project_id) REFERENCES project (project_id);

-- Version: 1.26
-- Description: Create table candidate
alter table positions
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.27
-- Description: Create table candidate
alter table grades
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.29
-- Description: Create table candidate
alter table request_types
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.31
-- Description: Create table candidate
alter table request
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (department_id) REFERENCES department (department_id),
    add FOREIGN KEY (request_type_id) REFERENCES request_types (request_type_id);

-- Version: 1.32
-- Description: Create table candidate
alter table vacations
    add FOREIGN KEY (request_type_id) REFERENCES request_types (request_type_id),
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (department_id) REFERENCES department (department_id);

-- Version: 1.33
-- Description: Create table candidate
alter table complaints
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (department_id) REFERENCES department (department_id);

-- Version: 1.34
-- Description: Create table candidate
alter table task_importance
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.35
-- Description: Create table candidate
alter table task_status
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.36
-- Description: Create table candidate
alter table tasks
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (department_id) REFERENCES department (department_id),
    add FOREIGN KEY (project_id) REFERENCES project (project_id),
    add FOREIGN KEY (team_id) REFERENCES team (team_id),
    add FOREIGN KEY (task_importance_id) REFERENCES task_importance (task_importance_id),
    add FOREIGN KEY (task_status_id) REFERENCES task_status (task_status_id);

-- Version: 1.37
-- Description: Create table candidate
alter table comments
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (task_id) REFERENCES tasks (task_id);

-- Version: 1.38
-- Description: Create table candidate
alter table vacancy
    add FOREIGN KEY (company_id) REFERENCES company (company_id);

-- Version: 1.39
-- Description: Create table candidate
alter table candidate
    add FOREIGN KEY (company_id) REFERENCES company (company_id),
    add FOREIGN KEY (vacancy_id) REFERENCES vacancy (vacancy_id);
