# Build the Go Binary.
FROM golang:1.18 as build_org-api
ENV CGO_ENABLED 0
ARG BUILD_REF

# Create the service directory and the copy the module files first and then
# download the dependencies. If this doesn't change, we won't need to do this
# again in future builds.
# RUN mkdir /service
# COPY go.* /service/
# WORKDIR /service
# RUN go mod download

# Copy the source code into the container.
COPY . /service

# Build the admin binary.
WORKDIR /service/app/tooling/org-admin
RUN go build -ldflags "-X main.build=${BUILD_REF}"

# Build the service binary.
WORKDIR /service/app/services/org-api
RUN go build -ldflags "-X main.build=${BUILD_REF}"


# Run the Go Binary in Alpine.
FROM alpine:3.15
ARG BUILD_DATE
ARG BUILD_REF
RUN addgroup -g 1000 -S sales && \
    adduser -u 1000 -h /service -G sales -S sales
COPY --from=build_org-api --chown=sales:sales /service/zarf/keys/. /service/zarf/keys/.
COPY --from=build_org-api --chown=sales:sales /service/app/tooling/org-admin/org-admin /service/org-admin
COPY --from=build_org-api --chown=sales:sales /service/app/services/org-api/org-api /service/org-api
WORKDIR /service
USER sales
CMD ["./org-api"]

LABEL org.opencontainers.image.created="${BUILD_DATE}" \
      org.opencontainers.image.title="org-api" \
      org.opencontainers.image.authors="William Kennedy <bill@ardanlabs.com>" \
      org.opencontainers.image.source="https://org/app/org-api" \
      org.opencontainers.image.revision="${BUILD_REF}" \
      org.opencontainers.image.vendor="Ardan Labs"