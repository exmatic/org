package web

import (
	"encoding/json"
	"gitlab.com/exmatic/org/business/sys/validate"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/dimfeld/httptreemux/v5"
)

// Param returns the web call parameters from the request.
func Param(r *http.Request, key string) string {
	m := httptreemux.ContextParams(r.Context())
	return m[key]
}

// ReadString is a helper method on application type that returns a string value from the URL query
// string, or the provided default value if no matching key is found.
func ReadString(qs url.Values, key string, defaultValue string) string {
	// Extract the value for a given key from the URL query string.
	// If no key exists this will return an empty string "".
	s := qs.Get(key)

	// If no key exists (or the value is empty) then return the default value
	if s == "" {
		return defaultValue
	}

	// Otherwise, return the string
	return s
}

// ReadCSV is a helper method on application type that reads a string value from the URL query
// string and then splits it into a slice on the comma character. If no matching key is found
// then it returns the provided default value.
func ReadCSV(qs url.Values, key string, defaultValue []string) []string {
	// Extract the value from the URL query string
	csv := qs.Get(key)

	// if no key exists (or the value is empty) then return the default value
	if csv == "" {
		return defaultValue
	}

	// Otherwise, parse the value into a []string slice and return it.
	return strings.Split(csv, ",")
}

// ReadInt is a helper method on application type that reads a string value from the URL query
// string and converts it to an integer before returning. If no matching key is found then it
// returns the provided default value. If the value couldn't be converted to an integer, then we
// record an error message in the provided Validator instance, and return the default value.
func ReadInt(qs url.Values, key string, defaultValue int, v *validate.Validator) int {
	// Extract the value from the URL query string.
	s := qs.Get(key)

	// If no key exists (or the value is empty) then return the default value.
	if s == "" {
		return defaultValue
	}

	// Try to convert the string value to an int. If this fails, add an error message to the
	// validator instance and return the default value.
	i, err := strconv.Atoi(s)
	if err != nil {
		v.AddError(key, "must be an integer value")
		return defaultValue
	}

	// Otherwise, return the converted integer value.
	return i
}

// Decode reads the body of an HTTP request looking for a JSON document. The
// body is decoded into the provided value.
//
// If the provided value is a struct then it is checked for validation tags.
func Decode(r *http.Request, val any) error {
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(val); err != nil {
		return err
	}

	return nil
}
